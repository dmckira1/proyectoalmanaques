<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('usuario_m');
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('dashboard_m');
    } //TERMINA EL CONSTRUCTOR

    // function index(){


    //     if($this->session->userdata('username')){//El userdate si es verdadero nos va a dirijir, si es falso no.
    //         redirect('welcome');

    //     }
    //     if(isset($_POST['password'])){//Si existe la variable enviada por el formulario
    //     $this->load->model('usuario_m');//Cargamos el modelo
    //     if($this->usuario_m->login($_POST['username'],md5($_POST['password']))){//Vamos a consultar. 
    //         $this->session->set_userdata('username',$_POST['username']);
    //         redirect('welcome');
    //     }
    //     else
    //     {
    //         redirect('login#bad-pass');
    //     }
    // }
    //     $this->load->view('usuario/Login');


    // }

    function index()
    {
        $this->load->view('inicio');
    }


    function fc_login()
    {
        $data = array(
            'CorreoUsuario' => $this->input->post('CorreoUsuario'),
            'ContrasenaUsuario' => md5($this->input->post('ContrasenaUsuario'))
        );

        $valusuarios = $this->usuario_m->validarusuario($data);

        if ($valusuarios) {
            $datossesion = array(
                'IdUsuario' => '',
                'RolUsuario' => '',
                'ApellidoUsuario' => '',
                'NombreUsuario' => '',
                'Numero' => '',
                'Direccion' => '',
                'EstadoUsuario' => '',
                'CorreoUsuario' => '',
                'ContrasenaUsuario' =>''
            );

            foreach ($valusuarios  as $valusuario) {
                $datossesion = array(
                    'IdUsuario' => $valusuario->Id_Usuario,
                    'RolUsuario' => $valusuario->Id_Rol,
                    'ApellidoUsuario' => $valusuario->Apellido,
                    'NombreUsuario' => $valusuario->Nombre,
                    'Numero' => $valusuario->Numero,
                    'Direccion' => $valusuario->Direccion,
                    'EstadoUsuario' => $valusuario->Id_Estado_Usuario,
                    'CorreoUsuario' => $valusuario->Correo,
                    'ContrasenaUsuario' => $valusuario->Contrasena
                );
            }
            $this->session->set_userdata($datossesion);

            echo (1);
        } else {
            echo (0);
        }
    }

    function cambiarpassw()
    {
        $data = array(
            'CorreoUsuario' => $this->input->post('CorreoUsuario'),
            'ContrasenaUsuario' => $this->input->post('ContrasenaUsuario'),
            'contrasenausuarion' => $this->input->post('contrasenausuarion')
            
        );

        $valusuarionpass = $this->usuario_m->validarusuariocambiarpass($data);

        if ($valusuarionpass) {
            $this->usuario_m->cambiarpassdefini($data);

            echo (1);
        } else {
            echo (0);
        }
    }

   

    function actualizarcontrasenac()
    {
        $id = $this->input->post('idusuario');
        $contrasena = $this->input->post('contrasena');
        $this->dashboard_m->actualizarcontrasenam($id, $contrasena);
    }

    public function logout()
    {
        // $variables = array('IdUsario','RolUsuario','ApellidoUsuario','NombreUsuario','EstadoUsuario','CorreoUsuario', 'ContrasenaUsuario');
        // $this->session_unset_userdata($variables);
        $this->session->sess_destroy();
        redirect('login');
    }

    function guardarUsuarioC()
    {
        
        

        $nombre = $this->input->post('nombre');
        $apellido = $this->input->post('apellido');
        $correo = $this->input->post('correo');
        $contraseña =  $this->input->post('contraseña');
        $verificar = $this->usuario_m->verificarsicorreoex($correo);
        if($verificar){
            echo (0);
        }
        else{

            $this->usuario_m->setUsuario($nombre, $apellido, $correo, $contraseña);
            echo(1);
        }
        
        
    }
    function politicasdeseguridad(){
        $datos['NombreUsuario'] = $this->session->userdata('NombreUsuario');
        $datos['ApellidoUsuario'] = $this->session->userdata('ApellidoUsuario');
        $datos['CorreoUsuario'] = $this->session->userdata('CorreoUsuario');
        $datos['IdUsuario'] = $this->session->userdata('IdUsuario');
        $datos['ContrasenaUsuario'] = $this->session->userdata('ContrasenaUsuario');
        $datos['Numero'] = $this->session->userdata('Numero');
        $datos['Direccion'] = $this->session->userdata('Direccion');
        $this->load->view('politicadatos', $datos);
    }

    function olvido()
    {
        $this->load->view('usuario/olvido');
    }
    function resetlink()
    {
        $email = $this->input->post('correo');
    }

    function vistaregistro()
    {
        $this->load->view('usuario/registrarse');
    }

    function vistaLogin()
    {
        $correo = $this->session->userdata('CorreoUsuario');
        
        $this->load->view('usuario/login', array('correo' => $correo));
    }

    function cargainicio()
    {
        $datos['NombreUsuario'] = $this->session->userdata('NombreUsuario');
        $datos['ApellidoUsuario'] = $this->session->userdata('ApellidoUsuario');
        $datos['CorreoUsuario'] = $this->session->userdata('CorreoUsuario');
        $datos['IdUsuario'] = $this->session->userdata('IdUsuario');
        $datos['ContrasenaUsuario'] = $this->session->userdata('ContrasenaUsuario');
        $datos['Numero'] = $this->session->userdata('Numero');
        $datos['Direccion'] = $this->session->userdata('Direccion');

        $this->load->view('inicio', $datos);
    }
    function actualizarperfilcc()
    {
        $id = $this->input->post('idusuario');
        $nombre = $this->input->post('nombre');
        $apellidos = $this->input->post('apellidos');
        $correo = $this->input->post('correo');
        $numero = $this->input->post('numero');
        $direccion = $this->input->post('direccion');
        $contrasena = $this->input->post('contrasena');


        $this->dashboard_m->actualizarperfil($id, $nombre, $apellidos, $correo, $numero, $direccion, $contrasena);
    }
    function vistacontactenos()
    {
        $datos['NombreUsuario'] = $this->session->userdata('NombreUsuario');
        $datos['ApellidoUsuario'] = $this->session->userdata('ApellidoUsuario');
        $datos['CorreoUsuario'] = $this->session->userdata('CorreoUsuario');
        $datos['IdUsuario'] = $this->session->userdata('IdUsuario');
        $datos['ContrasenaUsuario'] = $this->session->userdata('ContrasenaUsuario');
        $datos['Numero'] = $this->session->userdata('Numero');
        $datos['Direccion'] = $this->session->userdata('Direccion');


        $this->load->view('contactenos', $datos);
    }

    function vistaquienessomos()
    {
        $datos['NombreUsuario'] = $this->session->userdata('NombreUsuario');
        $datos['ApellidoUsuario'] = $this->session->userdata('ApellidoUsuario');
        $datos['CorreoUsuario'] = $this->session->userdata('CorreoUsuario');
        $datos['IdUsuario'] = $this->session->userdata('IdUsuario');
        $datos['ContrasenaUsuario'] = $this->session->userdata('ContrasenaUsuario');
        $datos['Numero'] = $this->session->userdata('Numero');
        $datos['Direccion'] = $this->session->userdata('Direccion');

        $this->load->view('quienessomos', $datos);
    }


    function productos()
    {

        $datos['NombreUsuario'] = $this->session->userdata('NombreUsuario');
        $datos['ApellidoUsuario'] = $this->session->userdata('ApellidoUsuario');
        $datos['CorreoUsuario'] = $this->session->userdata('CorreoUsuario');
        $datos['IdUsuario'] = $this->session->userdata('IdUsuario');
        $datos['ContrasenaUsuario'] = $this->session->userdata('ContrasenaUsuario');
        $datos['Numero'] = $this->session->userdata('Numero');
        $datos['Direccion'] = $this->session->userdata('Direccion');
        $esconder = '';
        
        $tablaProductos = $this->dashboard_m->listaproductosv();


        $datos['datosTablaProducto'] = "";
         

        $baseimagen =  base_url('');
        
        
        foreach ($tablaProductos as $valor) {

            if($datos['IdUsuario'] =='')
            {
                $esconder = 'Style ="display:none"';
            }
            


            $datos['datosTablaProducto']  .='<br>'
            .'<div class="contenedor1">'
        . '<div class="">'
               . '<img src="'.$baseimagen.'/resources/imagenesproductos/'.$valor->Imagen.'" alt="" width="200" height="200"><hr>'
               . '<h5><b>Referencia:</b></h5>'
               . '<input readonly type="text" style="background-color:transparent; border: 0;" id="referenciapp" name="referenciapp" value="' . $valor->Referencia . '"> <br><hr>'
               . '<h5><b>Nombre:</b></h5>'
               . '<input readonly type="text" style="background-color:transparent; border: 0;" id="nombrepp" name="nombrepp" value="' . $valor->Nombre . '"> <br><hr>'
               
           . '</div>'

           . '<div>'
               . '<h5><b>Categoría:</b></h5>'
               . '<input readonly type="text" id="catepp" name="catepp" style="background-color:transparent; border: 0;" value="' . $valor->Nombre_categoria . '"> <br><hr>'

               . '<h5><b>Valor unitario:</b></h5>'
               . '<input readonly type="text" id="punitariopp'. $valor->Id_Producto . '" name="punitariopp" style="background-color:transparent; border: 0;" value="' . $valor->Precio_Unitario . '" > <br><hr>'
               

               . '<h5><b>Cantidad:</b></h5>'
               . '<input  type="text" class="col-sm-2" style="border: 0px;" id="cantidadpp' . $valor->Id_Producto . '" name="cantidadpp" onkeyup="valortotal(' . $valor->Id_Producto . '); campovaciop('. $valor->Id_Producto .')"  onkeypress="return soloNumeros(event)"><br><hr>'

               . '<h5><b>Valor total:</b></h5>'
               . '<input readonly class="" type="text" id="valorpp'. $valor->Id_Producto . '" name="valorpp" readonly style="background-color:transparent; border: 0;" ><br><hr><br>'
                
               . '<button readonly type="button" '.$esconder.' id="btnap'. $valor->Id_Producto .'" name="btnap" onclick="agregarproducto(' . $valor->Id_Producto . ')" disabled="disabled"  class="btn btn-primary btn-sm offset-2">Agregar</button>'

               . '<button readonly type="button" name="btninformacionp" id="btninformacionp" onclick="vistaverproducto(' . $valor->Id_Producto . '), sumarvalorpedido(' . $valor->Id_Producto . ') "  class="btn btn-success btn-sm offset-1">Ver detalles</button>'

            . '</div>'
        . '</div> <br>';
            
        }
        $this->load->view('productos', $datos);
}


    function vistaverproducto(){
        $datos['NombreUsuario'] = $this->session->userdata('NombreUsuario');
        $datos['ApellidoUsuario'] = $this->session->userdata('ApellidoUsuario');
        $datos['CorreoUsuario'] = $this->session->userdata('CorreoUsuario');
        $datos['IdUsuario'] = $this->session->userdata('IdUsuario');
        $datos['ContrasenaUsuario'] = $this->session->userdata('ContrasenaUsuario');
        $datos['Numero'] = $this->session->userdata('Numero');
        $datos['Direccion'] = $this->session->userdata('Direccion');
        $idproducto = $this->input->get('idproducto');
        $verpc = $this->dashboard_m->informacionpcm($idproducto);

        foreach ($verpc as $valor) {
            $datos['Referencia'] = $valor->Referencia;
            $datos['Nombre'] = $valor->Nombre;
            $datos['Descripcion'] = $valor->Descripcion;
            $datos['Precio_Unitario'] = $valor->Precio_Unitario;
            $datos['Imagen'] = $valor->Imagen;
            $datos['Nombre_Categoria'] = $valor->Nombre_Categoria;
        }
        $this->load->view('verproducto',$datos);
    }

    function UsuarioDashboard()
    {
        $NombreU = $this->session->userdata('NombreUsuario');
        $this->load->view('dashboard/inicioAdmin', array('NombreUsuario' => $NombreU));
    }
    // function index()
    //     {

    //         if(isset($_POST['Contrasena'])){//Validamos con el iffset si la contraseña existe.
    //         $this->load->model('usuarios_m');
    //             if($this->usuarios_m->login($_POST['Correo'],$_POST['Contrasena']))
    //             {
    //                redirect('welcome');
    //             }else{
    //                 redirect('bad-pass');
    //             }
    //         }
    //         // $this->usuarios_m->login();
    //         $this->load->view('usuario/login');
    //     }



    function olvidocontrasena()
    {


    }

    

    function agregarproductoc(){
        $idusuario = $this->input->post('idusuario');
        $idproducto = $this->input->post('idproducto');
        $valor = $this->input->post('valor');
        $cantidad = $this->input->post('cantidad');
        
        $this->dashboard_m->agregarproductom($idusuario,$idproducto,$valor,$cantidad);
    }

    function consultarpedido(){
        $datos['NombreUsuario'] = $this->session->userdata('NombreUsuario');
        $datos['ApellidoUsuario'] = $this->session->userdata('ApellidoUsuario');
        $datos['CorreoUsuario'] = $this->session->userdata('CorreoUsuario');
        $datos['IdUsuario'] = $this->session->userdata('IdUsuario');
        $datos['ContrasenaUsuario'] = $this->session->userdata('ContrasenaUsuario');
        $datos['Numero'] = $this->session->userdata('Numero');
        $datos['Direccion'] = $this->session->userdata('Direccion');
        $id_usuario = $this->session->userdata('IdUsuario');        
        $tablaPedido = $this->dashboard_m->consultarpedidom($id_usuario);
        $tablavalort = $this->dashboard_m->sumarvalorpedidom($id_usuario);


        $datos['datosTablaPedido'] = "";
        $datos['datosTablaValort'] = "";
         

        $valorTotalProductoss = 0;
        foreach ($tablaPedido as $valor) {
            $TotalValor = $valor->Valor_Total;
            $datos['datosTablaPedido']  .='<tr>'
            . '<td align="center" id="" name="" style="background-color: #E9F3F7;">' . $valor->Referencia . '</td>'
            . '<td align="center" id="" name="" style="background-color: #E9F3F7;">' . $valor->Categoria . '</td>'
            . '<td align="center" id="referenciapp" name="referenciapp" style="background-color: #E9F3F7;" style="background-color: #E9F3F7;">' . $valor->Nombre . '</td>'
            . '<td align="center" id="punitariop" name="punitariop" style="background-color: #E9F3F7;">' ."$ " . ' ' . number_format($valor->Precio_Unitario, 2) . '</td>'       
            . '<td align="center" id="nombrepp" name="nombrepp" style="background-color: #E9F3F7;">' . $valor->Cantidad . '</td>'   
            . '<td align="center" id="nombrepp" name="nombrepp" style="background-color: #E9F3F7;">' ."$" . ' ' . number_format($TotalValor, 2) . '</td>'
            . '<td align="center" id=""style="background-color: #E9F3F7;">'
            . '<a class="btn"  type="button"  onclick="eliminarmipedido('. $valor->Id_Pedido .')"><i class="far fa-trash-alt col-sm-4 eliminari" id="" style="color:red" href=""></i></a>'
            . '<a class="btn"  type="button" data-toggle="modal" data-target="#modal20" data-id_pedido="'. $valor->Id_Pedido .'" data-cantidadpm="'. $valor->Cantidad .'" data-referenciapm="'. $valor->Referencia .'" data-categoriapm="'. $valor->Categoria .'" data-precioupm="'. $valor->Precio_Unitario .'" ><i class="far fa-edit editari offset-6" style="color:blue"></i></a></td></tr>';

            
            $valorTotalProductoss = $valorTotalProductoss + $valor->Valor_Total;
            $datos['echovalortotal'] = $valorTotalProductoss;
            
            // <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal1">Editar Perfil</a>
        }
        if($valorTotalProductoss==0){
            $datos['echovalortotal'] = 0;
        }

        
        $this->load->view('mipedido', $datos);
    }

    function eliminarmipedidoc(){
       $idpedido = $this->input->post('pedidoid');
       $this->dashboard_m->eliminarmipedidom($idpedido);
    }
    
    function confirmarpedidoc(){

        $idusuario = $this->input->post('idusuario');
        

        $this->dashboard_m->confirmarpedidom($idusuario);

    }

    function actualizarcmodalc(){
        $idpedido = $this->input->post('idpedido');
        $cantidad = $this->input->post('cantidad');
        $valor = $this->input->post('valor');
        
        $this->dashboard_m->actualizarcmodalm($idpedido, $cantidad, $valor);
        
    }


    function pruebai(){
        $this->load->view('pruebaimagen');
    }

   

    public function recuperarpass(){
        
        $query = $this->usuario_m->get_recordarPass($this->input->post('correo')); //Aqui voy al modelo y hago un select * from where documento a la tabla del login y me trae, documento, nombre de la persona
    
        // $pass = $this->usuario_m->get_validaLogin($this->input->post('correo')); // Aqui hago el mismo select pero solo me traigo la contraseña encriptada y la guardo en la variable $pass para desencriptarla
    
        // json_encode(array('correo' => $query->Correo, 'pass' => $query->Contrasena, 'nombre' => $query->Nombre . ' ' . $query->Apellido ));

      if (!is_null($query)) {
        echo json_encode (array('correo' => $query->Correo, 'pass' => $query->Contrasena, 'nombre' => $query->Nombre . ' ' . $query->Apellido ));
      } else {
   
       echo json_encode (1);
      }
    }

    public function fc_mailrecordarpass()
{
    
    //Recibo por post las variables q vienen desde la funcion js
    $nombre = $this->input->post('vnombre');
    $cmailusuaior = '
        <br><br>Hola!. ' . $nombre . '<br><br>De acuerdo a tu solicitud, te enviamos un enlace para cambiar la contraseña y acceder a la plataforma.
        <br><br>Por favor <b>presiona clic en el enlace "Cambiar contraseña" </b> para restablecer tu contraseña, en caso tal de que no haya requerido el cambio
        <br><br> le solicitamos que haga caso omiso a este correo.
        <br><br>Almanaques Carlos Velez.
        <br><br><b>Enlace : </br><a href="http://localhost/ProyectoAlmanaques/index.php/login/vistarecuperarpass?vcorreo='. $this->input->post('vcorreo').'&number=' . $this->input->post('contrasena').'"><span class="btn btn-primary">Cambiar contraseña</span></a>';
        
    
        //cargamos la libreria email de ci
        $this->load->library("email");

        $configGmail = array(
            'smtp_crypto' => 'tls', 
            'protocol' => 'smtp', 
            'smtp_host' => 'smtp.office365.com',
            'smtp_port' => 587,
            'smtp_user' => 'danielmc09@hotmail.com',
            'smtp_pass' => '08070325dmckira',
            'mailtype' => 'html',
        //'charset' => 'iso-8859-1',
            'charSet' => 'utf-8',
            'crlf' => "\r\n",
            'newline' => "\r\n"
    );
        

        //cargamos la configuracion para enviar con gmails
        $this->email->initialize($configGmail);
        //fin nuevo

        $this->email->from('danielmc09@hotmail.com', 'Recordatorio de contraseña - Almanaques carlos velez');
        $this->email->to($this->input->post('vcorreo'));
    
        $this->email->subject('Notificacion: Recordatorio');
        $this->email->message($cmailusuaior);
        $this->email->send();
        //con esto podemos ver el resultado
        var_dump($this->email->print_debugger());



    echo 0;


    
    }
    
    function vistarecuperarpass(){

        $this->load->view('usuario/loginrecuperar');
    }

    function actualizarpassuc()
    {
        $data = array(
            'usuarioid' => $this->input->post('usuarioid'),
            'contrasenaactual' => $this->input->post('contrasenaactual'),
            'contrasenanueva' => md5($this->input->post('contrasenanueva'))
            
        );

        $valusuarionpass = $this->usuario_m->validarcambiarpassum($data);

        if ($valusuarionpass) {
            $this->usuario_m->cambiarpassum($data);

            echo (1);
        } else {
            echo (0);
        }
    }

    function enproceso(){
        $datos['NombreUsuario'] = $this->session->userdata('NombreUsuario');
        $datos['ApellidoUsuario'] = $this->session->userdata('ApellidoUsuario');
        $datos['CorreoUsuario'] = $this->session->userdata('CorreoUsuario');
        $datos['IdUsuario'] = $this->session->userdata('IdUsuario');
        $datos['ContrasenaUsuario'] = $this->session->userdata('ContrasenaUsuario');
        $datos['Numero'] = $this->session->userdata('Numero');
        $datos['Direccion'] = $this->session->userdata('Direccion');
        $id_usuario = $this->session->userdata('IdUsuario');        
        $tablaPedido = $this->dashboard_m->consultarpedidoenprocesom($id_usuario);
        $tablavalort = $this->dashboard_m->sumarvalorpedidom($id_usuario);


        $datos['datosTablaPedido'] = "";
        $datos['datosTablaValort'] = "";
         

        $valorTotalProductoss = 0;
        foreach ($tablaPedido as $valor) {
            $TotalValor = $valor->Valor_Total;
            $datos['datosTablaPedido']  .='<tr>'
            . '<td align="center" id="" name="" style="background-color: #E9F3F7;">' . $valor->Referencia . '</td>'
            . '<td align="center" id="" name="" style="background-color: #E9F3F7;">' . $valor->Categoria . '</td>'
            . '<td align="center" id="referenciapp" name="referenciapp" style="background-color: #E9F3F7;" style="background-color: #E9F3F7;">' . $valor->Nombre . '</td>'     
            . '<td align="center" id="nombrepp" name="nombrepp" style="background-color: #E9F3F7;">' . $valor->Cantidad . '</td>'   
            . '</td></tr>';

            
            
            
            // <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal1">Editar Perfil</a>
        }
        

        
        $this->load->view('enproceso', $datos);
    }

    function despachados(){
        $datos['NombreUsuario'] = $this->session->userdata('NombreUsuario');
        $datos['ApellidoUsuario'] = $this->session->userdata('ApellidoUsuario');
        $datos['CorreoUsuario'] = $this->session->userdata('CorreoUsuario');
        $datos['IdUsuario'] = $this->session->userdata('IdUsuario');
        $datos['ContrasenaUsuario'] = $this->session->userdata('ContrasenaUsuario');
        $datos['Numero'] = $this->session->userdata('Numero');
        $datos['Direccion'] = $this->session->userdata('Direccion');
        $id_usuario = $this->session->userdata('IdUsuario');        
        $tablaPedido = $this->dashboard_m->consultarpedidodespachadom($id_usuario);
        $tablavalort = $this->dashboard_m->sumarvalorpedidom($id_usuario);


        $datos['datosTablaPedido'] = "";
        $datos['datosTablaValort'] = "";
         

        $valorTotalProductoss = 0;
        foreach ($tablaPedido as $valor) {
            $TotalValor = $valor->Valor_Total;
            $datos['datosTablaPedido']  .='<tr>'
            . '<td align="center" id="" name="" style="background-color: #E9F3F7;">' . $valor->Referencia . '</td>'
            . '<td align="center" id="" name="" style="background-color: #E9F3F7;">' . $valor->Categoria . '</td>'
            . '<td align="center" id="referenciapp" name="referenciapp" style="background-color: #E9F3F7;" style="background-color: #E9F3F7;">' . $valor->Nombre . '</td>'     
            . '<td align="center" id="nombrepp" name="nombrepp" style="background-color: #E9F3F7;">' . $valor->Cantidad . '</td>'   
            . '</td></tr>';

            
            
            
            // <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal1">Editar Perfil</a>
        }
        

        
        $this->load->view('despachados', $datos);
    }
    
    
}

