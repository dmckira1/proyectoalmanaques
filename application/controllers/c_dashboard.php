<?php
class C_dashboard extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('usuario_m');
        $this->load->model('dashboard_m');
        $this->load->helper('url');
        $this->load->library('session');
        if (!$this->session->userdata('IdUsuario')) { //Esta condicion sirve para que cuando le demos atras o pegemos un link, si no esta iniciada la sesion no le muestre nada de lo que tenemos en el proyecto
            redirect('login');
        }
    }

    function index()
    {
        if($this->session->userdata('RolUsuario') == 3){
            redirect('c_dashboard/index');
        }

        $datos['NombreUsuario'] = $this->session->userdata('NombreUsuario');
        $datos['ApellidoUsuario'] = $this->session->userdata('ApellidoUsuario');
        $datos['CorreoUsuario'] = $this->session->userdata('CorreoUsuario');
        $datos['IdUsuario'] = $this->session->userdata('IdUsuario');
        $datos['ContrasenaUsuario'] = md5($this->session->userdata('ContrasenaUsuario'));
        $datos['Numero'] = $this->session->userdata('Numero');
        $datos['Direccion'] = $this->session->userdata('Direccion');
        $contarad = $this->dashboard_m->contaradmin();
        $datos['contarad'] = $contarad;

        $this->load->view('dashboard/inicioDashboard_v', $datos);
        // $datos['contarad'] = $this->dashboar_m->contaradmin();
    }

    function Perfil()
    {
        $datos['NombreUsuario'] = $this->session->userdata('NombreUsuario');
        $datos['ApellidoUsuario'] = $this->session->userdata('ApellidoUsuario');
        $datos['CorreoUsuario'] = $this->session->userdata('CorreoUsuario');
        $datos['IdUsuario'] = $this->session->userdata('IdUsuario');
        $datos['ContrasenaUsuario'] = $this->session->userdata('ContrasenaUsuario');
        $datos['Numero'] = $this->session->userdata('Numero');
        $datos['Direccion'] = $this->session->userdata('Direccion');

        $this->load->view('dashboard/perfilDashboard_v', $datos);
    }
    function actualizarperfilcc()
    {
        $id = $this->input->post('idusuario');
        $nombre = $this->input->post('nombre');
        $apellidos = $this->input->post('apellidos');
        $correo = $this->input->post('correo');
        $numero = $this->input->post('numero');
        $direccion = $this->input->post('direccion');

        $this->dashboard_m->actualizarperfil($id, $nombre, $apellidos, $correo, $numero, $direccion);
    }
    function actualizarcontrasenac()
    {
        $id = $this->input->post('idusuario');
        $contrasena = md5($this->input->post('contrasena'));
        $this->dashboard_m->actualizarcontrasenam($id, $contrasena);
    }

    function logout()
    {
        // $variables = array('IdUsario','RolUsuario','ApellidoUsuario','NombreUsuario','EstadoUsuario','CorreoUsuario', 'ContrasenaUsuario');
        // $this->session_unset_userdata($variables);
        $this->session->sess_destroy();
        redirect('login');
    }
     

    function actualizarperfilc()
    {
        $id = $this->input->post('usuarioid');
        $nombre = $this->input->post('nombre');
        $apellidos = $this->input->post('apellidos');
        $contrasena = $this->input->post('contrasena');
        $this->dashboard_m->actualizarperfil($id, $nombre, $apellidos, $contrasena);
    }

    function Usuario()
    {
        if($this->session->userdata('RolUsuario') == 2){
            redirect('c_dashboard/index');
        }
        elseif($this->session->userdata('RolUsuario') == 3){
            redirect('login/cargainicio');
        }
        $NombreUsuario = $this->session->userdata('NombreUsuario');
        $ApellidoUsuario = $this->session->userdata('ApellidoUsuario');
        $CorreoUsuario = $this->session->userdata('CorreoUsuario');
        $IdUsuario = $this->session->userdata('IdUsuario');
        $ContrasenaUsuario = $this->session->userdata('ContrasenaUsuario');
        $Numero = $this->session->userdata('Numero');
        $Direccion = $this->session->userdata('Direccion');
        $tablaUsuarios = $this->dashboard_m->listaUsuarios();


        $datosTablaUsuario = '';


        foreach ($tablaUsuarios as $valor) {

            // ++$cont;
            // ++$a;
            // $b = $a % 2;
            // //para colocar el color de la fila
            // if($b === 1)
            // {
            //     $colorfila = 'class="success text-uppercase "';
            // }
            // else
            // {
            //     $colorfila = 'class="text-uppercase"';
            // }


            //fin deshabilita 

            //estaado de la encuesta


            //fin estado de la encuensta

            $datosTablaUsuario .=  '<tr>'
                . '<td style="background-color: #E9F3F7; color:#000;">' . $valor->Id_Usuario . '</td>'
                . '<td style="background-color: #E9F3F7; color:#000;">' . $valor->Rol . '</td>'
                . '<td style="background-color: #E9F3F7; color:#000;">' . $valor->Numero . '</td>'
                . '<td style="background-color: #E9F3F7; color:#000;">' . $valor->Direccion . '</td>'
                . '<td style="background-color: #E9F3F7; color:#000;">' . $valor->Nombre . '</td>'
                . '<td style="background-color: #E9F3F7; color:#000;">' . $valor->Apellido . '</td>'
                . '<td style="background-color: #E9F3F7; color:#000;">' . $valor->Correo . '</td>'
                . '<td style="background-color: #E9F3F7; color:#000;"><a class="btn"  onclick="irvistaEditarUsuario(' . $valor->Id_Usuario . ')"><i class="far fa-edit editari" style="color:blue"></i></a>'
                . '<a class="btn " type="button"  onclick="eliminarusuario(' . $valor->Id_Usuario . ')"><i class="far fa-trash-alt eliminari" style="color:red;"></i></a></td></tr>';

            //                    .'<td>'.$valor->documento_profesional.'</td>'
            // .'<td><button type="button" id="editar"  onclick="verrequerimiento('.$valor->folio.','.$valor->id.')" class="btn btn-outline-primary"><span class="glyphicon glyphicon-edit"></span> Ver </button></td>'
            // .'<td><button type="button" id="editar"  onclick="editararoportunidad('.$comilla.$valor->idcaso.$comilla.','.$comilla.$valor->estado.$comilla.')" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-edit"></span> Ver </button></td>'
        }



        // );
        $this->load->view('dashboard/usuarioDashboard_v', array(
            'NombreUsuario' => $NombreUsuario,
            'ApellidoUsuario' => $ApellidoUsuario,
            'datosTablaUsuario' => $datosTablaUsuario,
            'CorreoUsuario' => $CorreoUsuario,
            'IdUsuario' => $IdUsuario,
            'ContrasenaUsuario' => $ContrasenaUsuario,
            'Numero' => $Numero,
            'Direccion' => $Direccion

        ));
    }

    function Usuarioi()
    {
        if($this->session->userdata('RolUsuario') == 2){
            redirect('c_dashboard/index');
        }
        elseif($this->session->userdata('RolUsuario') == 3){
            redirect('login/cargainicio');
        }
        $NombreUsuario = $this->session->userdata('NombreUsuario');
        $ApellidoUsuario = $this->session->userdata('ApellidoUsuario');
        $CorreoUsuario = $this->session->userdata('CorreoUsuario');
        $IdUsuario = $this->session->userdata('IdUsuario');
        $ContrasenaUsuario = $this->session->userdata('ContrasenaUsuario');
        $Numero = $this->session->userdata('Numero');
        $Direccion = $this->session->userdata('Direccion');
        $tablaUsuarios = $this->dashboard_m->listaUsuariosi();


        $datosTablaUsuario = '';


        foreach ($tablaUsuarios as $valor) {


            $datosTablaUsuario .=  '<tr>'
                . '<td style="background-color: #E9F3F7; color:#000;">' . $valor->Id_Usuario . '</td>'
                . '<td style="background-color: #E9F3F7; color:#000;">' . $valor->Rol . '</td>'
                . '<td style="background-color: #E9F3F7; color:#000;">' . $valor->Numero . '</td>'
                . '<td style="background-color: #E9F3F7; color:#000;">' . $valor->Direccion . '</td>'
                . '<td style="background-color: #E9F3F7; color:#000;">' . $valor->Nombre . '</td>'
                . '<td style="background-color: #E9F3F7; color:#000;">' . $valor->Apellido . '</td>'
                . '<td style="background-color: #E9F3F7; color:#000;">' . $valor->Correo . '</td>'
                . '<td style="background-color: #E9F3F7; color:#000;" align="center"><a  class="btn"  onclick="irvistaEditarUsuario(' . $valor->Id_Usuario . ')"><i class="far fa-edit editari" style="color:blue"></i></a><br>'
                . '<a class="btn " type="button"  onclick="eliminarusuario(' . $valor->Id_Usuario . ')"><i class="far fa-trash-alt  eliminari" id="" style="color:red;" href=""></i></a></td></tr>';

            //                    .'<td>'.$valor->documento_profesional.'</td>'
            // .'<td><button type="button" id="editar"  onclick="verrequerimiento('.$valor->folio.','.$valor->id.')" class="btn btn-outline-primary"><span class="glyphicon glyphicon-edit"></span> Ver </button></td>'
            // .'<td><button type="button" id="editar"  onclick="editararoportunidad('.$comilla.$valor->idcaso.$comilla.','.$comilla.$valor->estado.$comilla.')" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-edit"></span> Ver </button></td>'
        }

        // );
        $this->load->view('dashboard/usuarioiDashboard_v', array(
            'NombreUsuario' => $NombreUsuario,
            'ApellidoUsuario' => $ApellidoUsuario,
            'datosTablaUsuario' => $datosTablaUsuario,
            'CorreoUsuario' => $CorreoUsuario,
            'IdUsuario' => $IdUsuario,
            'ContrasenaUsuario' => $ContrasenaUsuario,
            'Numero' => $Numero,
            'Direccion' => $Direccion

        ));
    }

    function eliminarusuarioc()
    {
        $id = $this->input->post('usuarioid');
        $estado = $this->input->post('estado');

        $this->dashboard_m->eliminarusuarioc($id, $estado);
    }
    function eliminarcategoriac()
    {
        $id = $this->input->post('categoriaid');
        $estado = $this->input->post('estado');

        $this->dashboard_m->eliminarcategoriam($id, $estado);
    }

    function eliminarproductoc()
    {
        $id = $this->input->post('productoid');
        $estado = $this->input->post('estado');

        $this->dashboard_m->eliminarproductom($id, $estado);
    }
    function cambiarnodisponiblec()
    {
        $id = $this->input->post('productoid');
        $estado = $this->input->post('estado');

        $this->dashboard_m->cambiarnodisponiblem($id, $estado);
    }

    function cambiardisponiblec()
    {
        $id = $this->input->post('productoid');
        $estado = $this->input->post('estado');

        $this->dashboard_m->cambiardisponiblem($id, $estado);
    }

    function vistaEditarUsuario()
    {
        if($this->session->userdata('RolUsuario') == 2){
            redirect('c_dashboard/index');
        }
        elseif($this->session->userdata('RolUsuario') == 3){
            redirect('login/cargainicio');
        }
        $datos['NombreUsuario'] = $this->session->userdata('NombreUsuario');
        $datos['ApellidoUsuario'] = $this->session->userdata('ApellidoUsuario');
        $datos['CorreoUsuario'] = $this->session->userdata('CorreoUsuario');
        $datos['IdUsuario'] = $this->session->userdata('IdUsuario');
        $datos['ContrasenaUsuario'] = $this->session->userdata('ContrasenaUsuario');
        $datos['Numero'] = $this->session->userdata('Numero');
        $datos['Direccion'] = $this->session->userdata('Direccion');


        $datos['idusuario'] = $this->input->get('idusuario');
        $idusuario = $this->input->get('idusuario');
        


        $datos['estadousuario'] = "";

        // //esta variable me trae el select que me entrega el modelo


        $datos['rolusuario'] = "";
        

        // //esta variable me trae el select que me entrega el modelo
        $traerrol = $this->dashboard_m->traerrol();

        // //recorro  la consulta que se guarda en la variable $traecategoria y la renombro , para meterle el id al value y el nombre al omption
        foreach ($traerrol as $traerrols) {
            $datos['rolusuario'] .= "<option value='$traerrols->Id_Rol' >$traerrols->Rol</option>";
            
        }
        // //la variable $mostrarcategoria


        $traerusuario = $this->dashboard_m->fmtraerusuario($idusuario);


        foreach ($traerusuario as $traerusers) {

            $datos['Id_Usuario'] = $traerusers->Id_Usuario;
            $datos['Id_Rol'] = $traerusers->Id_Rol;
            $datos['Rol'] = $traerusers->Rol;
            $datos['Numero'] = $traerusers->Numero;
            $datos['Direccion'] = $traerusers->Direccion;
            $datos['Id_Estado_Usuario'] = $traerusers->Id_Estado_Usuario;
            $datos['Estado'] = $traerusers->Estado;
            $datos['Nombre'] = $traerusers->Nombre;
            $datos['Apellido'] = $traerusers->Apellido;
            $datos['Correo'] = $traerusers->Correo;
            // $datos['Nombre'] = $traerusers->Nombre;


            // $arrayproducto = array(
            //     'Id_Producto' => $traerproducts->Id_Producto,
            //     'Id_Estado_Producto' => $traerproducts->Id_Estado_Producto,
            //     'Id_Categoria' => $traerproducts->Id_Categoria,
            //     'nombre_categoria' => $traerproducts->nombre_categoria,
            //     'nombre_estado' => $traerproducts->nombre_estado,
            //     'Referencia' => $traerproducts->Referencia,
            //     'Nombre' => $traerproducts->Nombre,
            //     'Descripcion' => $traerproducts->Descripcion,
            //     'Imagen' => $traerproducts->Imagen,
            // );
        }


        $traerestado = $this->dashboard_m->traerestado();

        if ($datos['Id_Estado_Usuario'] == '') {
            $traeopsion = "<option value=''>Seleccione una opción</option>";
        } else {
            $traeopsion = '<option value="' . $datos['Id_Estado_Usuario'] . '"> ' . $datos['Estado'] . ' </option>';
        }

        // //recorro  la consulta que se guarda en la variable $traecategoria y la renombro , para meterle el id al value y el nombre al omption
        foreach ($traerestado as $traerestados) {
            $traeopsion  .= "<option value='$traerestados->Id_Estado_Usuario' >$traerestados->Estado</option>";
        }

        $datos['traeopsion'] = $traeopsion;



        $traerrol = $this->dashboard_m->traerrol();

        if ($datos['Id_Rol'] == '') {
            $traeopcionrol = "<option value=''>Seleccione una opción</option>";
        } else {
            $traeopcionrol = '<option value="' . $datos['Id_Rol'] . '"> ' . $datos['Rol'] . ' </option>';
        }

        // //recorro  la consulta que se guarda en la variable $traecategoria y la renombro , para meterle el id al value y el nombre al omption
        foreach ($traerrol as $traerrols) {
            $traeopcionrol  .= "<option value='$traerrols->Id_Rol' >$traerrols->Rol</option>";
        }

        $datos['traeopcionrol'] = $traeopcionrol;

        $this->load->view('dashboard/editarUsuarioDashboard_v', $datos);
    }

    function cambiardeestadousuarioc()
    {
        $id = $this->input->post('idusuario');
        $estado = $this->input->post('estado');
        $this->dashboard_m->cambiardeestadousuariom($id, $estado);
    }

    function Producto()
    {
        if($this->session->userdata('RolUsuario') == 2){
            redirect('c_dashboard/index');
        }
        elseif($this->session->userdata('RolUsuario') == 3){
            redirect('login/cargainicio');
        }
        $NombreUsuario = $this->session->userdata('NombreUsuario');
        $ApellidoUsuario = $this->session->userdata('ApellidoUsuario');
        $CorreoUsuario = $this->session->userdata('CorreoUsuario');
        $IdUsuario = $this->session->userdata('IdUsuario');
        $ContrasenaUsuario = $this->session->userdata('ContrasenaUsuario');
        $Numero = $this->session->userdata('Numero');
        $Direccion = $this->session->userdata('Direccion');
        $tablaProductos = $this->dashboard_m->listaProductos();


        $datosTablaProducto = '';


        $baseimagen =  base_url('');
        foreach ($tablaProductos as $valor) {

            $datosTablaProducto .=  '<tr>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Id_Producto . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Id_Categoria . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Precio_Unitario . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Referencia . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Nombre . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Descripcion . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;"><img src="'.$baseimagen.'/resources/imagenesproductos/'.$valor->Imagen.'" alt="" width="100" height="100"></td>'
                //                    .'<td>'.$valor->documento_profesional.'</td>'
                //     .'<td><button type="button" id="editarP" onclick="editarproducto('.$valor->Id_Producto.')"  class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-edit"></span> Editar </button>'
                // .'<button type="button" id="editarP"   class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-edit"></span> Eliminar </button></td>'

                . '<td align="center" id="" style="background-color: #E9F3F7; color:#000;"><a class="btn "  onclick="irvistaEditarProducto(' . $valor->Id_Producto . ')"  ><i class="far fa-edit editari offset-6" style="color:blue"></i></a><br>'
                . '<a class="btn"  type="button"  onclick="eliminarproducto(' . $valor->Id_Producto . ')"><i class="far fa-trash-alt col-sm-4 eliminari" id="" style="color:red" href=""></i></a><br>'
                . '<a class="btn"  type="button"  onclick="cambiarnodisponible(' . $valor->Id_Producto . ')"><i class="fas fa-power-off col-sm-6" style="color:#FA9506"></i></a></td></tr>';
        }

        // );
        $this->load->view('dashboard/productoDashboard_v', array(
            'NombreUsuario' => $NombreUsuario,
            'ApellidoUsuario' => $ApellidoUsuario,
            'datosTablaProducto' => $datosTablaProducto,
            'CorreoUsuario' => $CorreoUsuario,
            'IdUsuario' => $IdUsuario,
            'ContrasenaUsuario' => $ContrasenaUsuario,
            'Numero' => $Numero,
            'Direccion' => $Direccion,
        ));
    }

    function ProductoNo()
    {
        if($this->session->userdata('RolUsuario') == 2){
            redirect('c_dashboard/index');
        }
        elseif($this->session->userdata('RolUsuario') == 3){
            redirect('login/cargainicio');
        }
        $NombreUsuario = $this->session->userdata('NombreUsuario');
        $ApellidoUsuario = $this->session->userdata('ApellidoUsuario');
        $CorreoUsuario = $this->session->userdata('CorreoUsuario');
        $IdUsuario = $this->session->userdata('IdUsuario');
        $ContrasenaUsuario = $this->session->userdata('ContrasenaUsuario');
        $Numero = $this->session->userdata('Numero');
        $Direccion = $this->session->userdata('Direccion');
        $tablaProductos = $this->dashboard_m->listaProductosn();


        $datosTablaProducto = '';

        $baseimagen =  base_url('');

        foreach ($tablaProductos as $valor) {

            $datosTablaProducto .=  '<tr>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Id_Producto . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Id_Categoria . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Precio_Unitario . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Referencia . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Nombre . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Descripcion . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;"><img src="'.$baseimagen.'/resources/imagenesproductos/'.$valor->Imagen.'" alt="" width="100" height="100"</td>'
                //                    .'<td>'.$valor->documento_profesional.'</td>'
                //     .'<td><button type="button" id="editarP" onclick="editarproducto('.$valor->Id_Producto.')"  class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-edit"></span> Editar </button>'
                // .'<button type="button" id="editarP"   class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-edit"></span> Eliminar </button></td>'

                . '<td align="center" id="" style="background-color: #E9F3F7; color:#000;"><a class="btn "  onclick="irvistaEditarProducto(' . $valor->Id_Producto . ')"><i class="far fa-edit editari col-sm-6" style="color:blue"></i></a><br>'
                . '<a class="btn"  type="button"  onclick="eliminarproducto(' . $valor->Id_Producto . ')"><i class="far fa-trash-alt eliminari col-sm-6" style="color:red"></i></a><br>'
                . '<a class="btn"  type="button"  onclick="cambiardisponible(' . $valor->Id_Producto . ')"><i class="fas fa-power-off col-sm-6" style="color:green"></i></a></td></tr>';
        }

        // );
        $this->load->view('dashboard/productoNoDashboard_v', array(
            'NombreUsuario' => $NombreUsuario,
            'ApellidoUsuario' => $ApellidoUsuario,
            'datosTablaProducto' => $datosTablaProducto,
            'CorreoUsuario' => $CorreoUsuario,
            'IdUsuario' => $IdUsuario,
            'ContrasenaUsuario' => $ContrasenaUsuario,
            'Numero' => $Numero,
            'Direccion' => $Direccion,
        ));
    }

    function vistaEditarProducto()
    {
        if($this->session->userdata('RolUsuario') == 2){
            redirect('c_dashboard/index');
        }
        elseif($this->session->userdata('RolUsuario') == 3){
            redirect('login/cargainicio');
        }
        $datos['NombreUsuario'] = $this->session->userdata('NombreUsuario');
        $datos['ApellidoUsuario'] = $this->session->userdata('ApellidoUsuario');
        $datos['CorreoUsuario'] = $this->session->userdata('CorreoUsuario');
        $datos['IdUsuario'] = $this->session->userdata('IdUsuario');
        $datos['ContrasenaUsuario'] = $this->session->userdata('ContrasenaUsuario');
        $datos['Numero'] = $this->session->userdata('Numero');
        $datos['Direccion'] = $this->session->userdata('Direccion');

        $datos['idproducto'] = $this->input->get('idproducto');
        $idproducto = $this->input->get('idproducto');

        //inicializo la categoria con valor vacio
        $datos['mostrarcategoria'] = "";

        //esta variable me trae el select que me entrega el modelo
        $traecategoria = $this->dashboard_m->traercategoria();

        //recorro  la consulta que se guarda en la variable $traecategoria y la renombro , para meterle el id al value y el nombre al omption
        foreach ($traecategoria as $traercategorias) {
            $datos['mostrarcategoria'] .= "<option value='$traercategorias->Id_Categoria' >$traercategorias->Nombre</option>";
        }
        //la variable $mostrarcategoria


        $traerproducto = $this->dashboard_m->fmtraerproducto($idproducto);


        foreach ($traerproducto as $traerproducts) {

            $datos['Id_Producto'] = $traerproducts->Id_Producto;
            $datos['Id_Estado_Producto'] = $traerproducts->Id_Estado_Producto;
            $datos['Id_Categoria'] = $traerproducts->Id_Categoria;
            $datos['nombre_categoria'] = $traerproducts->nombre_categoria;
            $datos['nombre_estado'] = $traerproducts->nombre_estado;
            $datos['Referencia'] = $traerproducts->Referencia;
            $datos['Nombre'] = $traerproducts->Nombre;
            $datos['Descripcion'] = $traerproducts->Descripcion;
            $datos['Precio_Unitario'] = $traerproducts->Precio_Unitario;
            $datos['Imagen'] = $traerproducts->Imagen;
        }


        $this->load->view('dashboard/editarProductoDashboard_v', $datos);
    }

    function subirfotosproducto()
    {
        if (!empty($_FILES['file']['name'])) {
            // Set preference
            $config['upload_path'] = FCPATH . "resources/imagenesproductos";
            $config['allowed_types'] = '*';
            $config['max_size'] = '30000'; // max_size in kb
            $config['file_name'] = $_FILES['file']['name'];


            //Load upload library
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file')) {

                $datos['error'] = $this->upload->display_errors();
            } else {
                $idproducto = $this->uri->segment(3);
                $datos = array(
                    'id_producto' => $idproducto,
                    'imagen' => $_FILES['file']['name'],
                );
                //ACA VA EL MODELO PARA GUARDAR LAS IMAGENES
                $this->dashboard_m->actualizarimagenes($datos);

                echo "Imagen subida";
            }
        }
    }

    function insertarfotosproducto()
    {
        if (!empty($_FILES['file']['name'])) {
            // Set preference
            $config['upload_path'] = FCPATH . "resources/imagenesproductos";
            $config['allowed_types'] = '*';
            $config['max_size'] = '30000'; // max_size in kb
            $config['file_name'] = $_FILES['file']['name'];


            //Load upload library
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('file')) {

                $datos['error'] = $this->upload->display_errors();
            } else {
                $idproducto = $this->uri->segment(3);
                $datos = array(
                    'id_producto' => $idproducto,
                    'imagen' => $_FILES['file']['name'],
                );
                //ACA VA EL MODELO PARA GUARDAR LAS IMAGENES
                $this->db->insertarimagenes('imagenesproducto', $datos);

                echo "Imagen subida";
            }
        }
    }

    //en esta funcion cargo la vista y le llevo la categoria a la vista y la imprimo
    function agregarproductoconi()
    {

        $mi_archivo = 'upload';
        $config['upload_path'] = "resources/imagenesproductos/";
        // $config['file_name'] = "nombre_archivo";
        $config['allowed_types'] = "jpg|jpeg|png";
        $config['max_size'] = "50000";
        $config['max_width'] = "2000";
        $config['max_height'] = "2000";

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($mi_archivo)) {
            //*** ocurrio un error
            $data['uploadError'] = $this->upload->display_errors();
            echo $this->upload->display_errors();
            return;
        }
        $data = $this->upload->data();
        $imagen = $data['file_name'];
        $categoria = $this->input->post('categoriaprueba');
        $referencia = $this->input->post('referenciaprueba');
        $descripcion = $this->input->post('descripcionprueba');
        $nombre = $this->input->post('nombreprueba');
        $preciounitario = $this->input->post('preciounitarioprueba');
        $data['uploadSuccess'] = $this->upload->data();
        $this->dashboard_m->cargarproductop($categoria, $referencia, $nombre, $descripcion, $preciounitario, $imagen);
        redirect('c_dashboard/Producto');
    }

    function actualizarproductoconi()
    {

        $mi_archivo = 'upload2';
        $config['upload_path'] = "resources/imagenesproductos/";
        // $config['file_name'] = "nombre_archivo";
        $config['allowed_types'] = "jpg|jpeg|png";
        $config['max_size'] = "50000";
        $config['max_width'] = "2000";
        $config['max_height'] = "2000";

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($mi_archivo)) {
            //*** ocurrio un error
            $data['uploadError'] = $this->upload->display_errors();
            echo $this->upload->display_errors();
            return;
        }
        $data = $this->upload->data();
        $imagen = $data['file_name'];
        $idproducto = $this->input->post('idproductoed');
        $categoria = $this->input->post('apcategoria');
        $referencia = $this->input->post('apreferencia');
        $descripcion = $this->input->post('apdescripcion');
        $nombre = $this->input->post('apnombre');
        $preciounitario = $this->input->post('apprecioun');
        $data['uploadSuccess'] = $this->upload->data();
        $this->dashboard_m->actualizarproductopi($idproducto, $categoria, $referencia, $nombre, $descripcion, $preciounitario, $imagen);
        redirect('c_dashboard/Producto');
    }


    function agregarProducto()
    {

        if($this->session->userdata('RolUsuario') == 2){
            redirect('c_dashboard/index');
        }
        elseif($this->session->userdata('RolUsuario') == 3){
            redirect('login/cargainicio');
        }
        $NombreUsuario = $this->session->userdata('NombreUsuario');
        $ApellidoUsuario = $this->session->userdata('ApellidoUsuario');
        $CorreoUsuario = $this->session->userdata('CorreoUsuario');
        $IdUsuario = $this->session->userdata('IdUsuario');
        $ContrasenaUsuario = $this->session->userdata('ContrasenaUsuario');
        $Numero = $this->session->userdata('Numero');
        $Direccion = $this->session->userdata('Direccion');

        // $datos['NombreUsuario'] = $this->session->userdata('NombreUsuario');
        // $datos['ApellidoUsuario'] = $this->session->userdata('ApellidoUsuario');
        // $datos['CorreoUsuario'] = $this->session->userdata('CorreoUsuario');
        // $datos['IdUsuario'] = $this->session->userdata('IdUsuario');
        // $datos['ContrasenaUsuario'] = $this->session->userdata('ContrasenaUsuario');
        // $datos['Numero'] = $this->session->userdata('Numero');
        // $datos['Direccion'] = $this->session->userdata('Direccion');
        //inicializo la categoria con valor vacio


        $mostrarcategoria = "<option value=''>Seleccione</option>";

        //esta variable me trae el select que me entrega el modelo
        $traecategoria = $this->dashboard_m->traercategoria();

        //recorro  la consulta que se guarda en la variable $traecategoria y la renombro , para meterle el id al value y el nombre al omption
        foreach ($traecategoria as $traercategorias) {
            $mostrarcategoria .= '<option value=' . $traercategorias->Id_Categoria . '">' . $traercategorias->Nombre . '</option>';
        }
        //la variable $mostrarcategoria



        $this->load->view('dashboard/agregarProductoDashboard_v', array(
            'NombreUsuario' => $NombreUsuario,
            'ApellidoUsuario' => $ApellidoUsuario,
            'mostrarcategoria' => $mostrarcategoria,
            'CorreoUsuario' => $CorreoUsuario,
            'IdUsuario' => $IdUsuario,
            'ContrasenaUsuario' => $ContrasenaUsuario,
            'Numero' => $Numero,
            'Direccion' => $Direccion

        ));
    }

    function guardarProductoC()
    {
        //var_dump('$data');
        //exit();
        $categoria = $this->input->post('categoria');
        $estado = $this->input->post('estado');
        $referencia = $this->input->post('referencia');
        $nombre = $this->input->post('nombre');
        $descripcion = $this->input->post('descripcion');
        $imagen = $this->input->post('imagen');
        $this->dashboard_m->setProducto($categoria, $estado, $referencia, $nombre, $descripcion, $imagen);
    }
    function actualizarProductoC()
    {
        $id = $this->input->post('id');
        $categoria = $this->input->post('categoria');
        $referencia = $this->input->post('referencia');
        $nombre = $this->input->post('nombre');
        $descripcion = $this->input->post('descripcion');
        $this->dashboard_m->actualizarProducto($id, $categoria, $referencia, $nombre, $descripcion);
    }

    function actualizarCategoriaC()
    {
        $id = $this->input->post('id');
        $nombre = $this->input->post('nombre');
        $descripcion = $this->input->post('descripcion');
        $this->dashboard_m->actualizarCategoriam($id, $nombre, $descripcion);
    }

    function Categorias()
    {
        if($this->session->userdata('RolUsuario') == 2){
            redirect('c_dashboard/index');
        }
        elseif($this->session->userdata('RolUsuario') == 3){
            redirect('login/cargainicio');
        }
        $NombreUsuario = $this->session->userdata('NombreUsuario');
        $ApellidoUsuario = $this->session->userdata('ApellidoUsuario');
        $CorreoUsuario = $this->session->userdata('CorreoUsuario');
        $IdUsuario = $this->session->userdata('IdUsuario');
        $ContrasenaUsuario = $this->session->userdata('ContrasenaUsuario');
        $Numero = $this->session->userdata('Numero');
        $Direccion = $this->session->userdata('Direccion');

        $tablaCategorias = $this->dashboard_m->listaCategorias();


        $datosTablaCategoria = '';


        foreach ($tablaCategorias as $valor) {

            $datosTablaCategoria .=  '<tr>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Id_Categoria . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Nombre . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Descripcion . '</td>'
                //                    .'<td>'.$valor->documento_profesional.'</td>'
                //     .'<td><button type="button" id="editarP" onclick="editarproducto('.$valor->Id_Producto.')"  class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-edit"></span> Editar </button>'
                // .'<button type="button" id="editarP"   class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-edit"></span> Eliminar </button></td>'

                . '<td align="center" id="" style="background-color: #E9F3F7; color:#000;"><a class="btn "  onclick="irvistaEditarCategoria(' . $valor->Id_Categoria . ')"  ><i class="far fa-edit editari" style="color:blue"></i></a>'
                . '<a class="btn"  type="button"  onclick="eliminarcategoria(' . $valor->Id_Categoria . ')"><i class="far fa-trash-alt col-sm-4 eliminari" id="" style="color:red" href=""></i></a></td>';;
        }

        $this->load->view('dashboard/categoriaDashboard_v', array(
            'NombreUsuario' => $NombreUsuario,
            'ApellidoUsuario' => $ApellidoUsuario,
            'datosTablaCategoria' => $datosTablaCategoria,
            'CorreoUsuario' => $CorreoUsuario,
            'IdUsuario' => $IdUsuario,
            'ContrasenaUsuario' => $ContrasenaUsuario,
            'Numero' => $Numero,
            'Direccion' => $Direccion

        ));
    }


    function vistaEditarCategoria()
    {
        if($this->session->userdata('RolUsuario') == 2){
            redirect('c_dashboard/index');
        }
        elseif($this->session->userdata('RolUsuario') == 3){
            redirect('login/cargainicio');
        }
        $datos['NombreUsuario'] = $this->session->userdata('NombreUsuario');
        $datos['ApellidoUsuario'] = $this->session->userdata('ApellidoUsuario');
        $datos['CorreoUsuario'] = $this->session->userdata('CorreoUsuario');
        $datos['IdUsuario'] = $this->session->userdata('IdUsuario');
        $datos['ContrasenaUsuario'] = $this->session->userdata('ContrasenaUsuario');
        $datos['Numero'] = $this->session->userdata('Numero');
        $datos['Direccion'] = $this->session->userdata('Direccion');
        $datos['idcategoria'] = $this->input->get('idcategoria');

        $idcategoria = $this->input->get('idcategoria');

        $traercategoriasv = $this->dashboard_m->categoriat($idcategoria);

        foreach ($traercategoriasv as $traercategoriasvs) {
            $datos['Nombre'] = $traercategoriasvs->Nombre;
            $datos['Descripcion'] = $traercategoriasvs->Descripcion;
        }
        $this->load->view('dashboard/editarCategoriaDashboard_v', $datos);
    }

    function agregarCategoria()
    {
        if($this->session->userdata('RolUsuario') == 2){
            redirect('c_dashboard/index');
        }
        elseif($this->session->userdata('RolUsuario') == 3){
            redirect('login/cargainicio');
        }
        $NombreUsuario = $this->session->userdata('NombreUsuario');
        $ApellidoUsuario = $this->session->userdata('ApellidoUsuario');
        $CorreoUsuario = $this->session->userdata('CorreoUsuario');
        $IdUsuario = $this->session->userdata('IdUsuario');
        $ContrasenaUsuario = $this->session->userdata('ContrasenaUsuario');
        $Numero = $this->session->userdata('Numero');
        $Direccion = $this->session->userdata('Direccion');

        //inicializo la categoria con valor vacio
        $mostrarcategoria = "<option value=''>Seleccione</option>";

        //esta variable me trae el select que me entrega el modelo
        $traecategoria = $this->dashboard_m->traercategoria();

        //recorro  la consulta que se guarda en la variable $traecategoria y la renombro , para meterle el id al value y el nombre al omption
        foreach ($traecategoria as $traercategorias) {
            $mostrarcategoria .= '<option value=' . $traercategorias->Id_Categoria . '">' . $traercategorias->Nombre . '</option>';
        }
        //la variable $mostrarcategoria



        $this->load->view('dashboard/agregarCategoriaDashboard_v', array(
            'NombreUsuario' => $NombreUsuario,
            'ApellidoUsuario' => $ApellidoUsuario,
            'mostrarcategoria' => $mostrarcategoria,
            'CorreoUsuario' => $CorreoUsuario,
            'IdUsuario' => $IdUsuario,
            'ContrasenaUsuario' => $ContrasenaUsuario,
            'Numero' => $Numero,
            'Direccion' => $Direccion

        ));
    }

    function guardarCategoriaC()
    {
        $categoria = $this->input->post('categoria');
        $descripcion = $this->input->post('descripcionn');
        $this->dashboard_m->setCategoria($categoria, $descripcion);
    }


    function actualizarusuariodesdeadminc()
    {
        $id = $this->input->post('usuarioid');
        $rol = $this->input->post('rol');
        $estado = $this->input->post('estado');
        $nombre = $this->input->post('nombre');
        $apellido = $this->input->post('apellidos');
        $correo = $this->input->post('correo');

        $this->dashboard_m->actualizarusuariodesdeadminm($id, $rol, $estado, $nombre, $apellido, $correo);
    }



    // CONTROLADORES DE VENTAS DASHBOARD


    function vistaconfirmarventa()
    {

        $NombreUsuario = $this->session->userdata('NombreUsuario');
        $ApellidoUsuario = $this->session->userdata('ApellidoUsuario');
        $CorreoUsuario = $this->session->userdata('CorreoUsuario');
        $IdUsuario = $this->session->userdata('IdUsuario');
        $ContrasenaUsuario = $this->session->userdata('ContrasenaUsuario');
        $Numero = $this->session->userdata('Numero');
        $Direccion = $this->session->userdata('Direccion');

        $tablaPedidosc = $this->dashboard_m->consultarpedidoc();


        $datosTablaPedidoc = '';


        foreach ($tablaPedidosc as $valor) {

            $datosTablaPedidoc .=  '<tr>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Id_Pedido . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Id_Usuario . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Nombre . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Correo . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Referencia . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Nombre_Producto . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Cantidad . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . number_format($valor->Valor_Total, 2) . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Fecha_Pedido . '</td>'
                //                    .'<td>'.$valor->documento_profesional.'</td>'
                //     .'<td><button type="button" id="editarP" onclick="editarproducto('.$valor->Id_Producto.')"  class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-edit"></span> Editar </button>'
                // .'<button type="button" id="editarP"   class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-edit"></span> Eliminar </button></td>'

                . '<td align="center" id="" style="background-color: #E9F3F7;"><a class="btn"  type="button"  onclick="confirmarpedidoad(' . $valor->Id_Pedido . ')"><i class="fas fa-check-circle col-sm-6" style="color:green"></i></a><br>'
                . '<a class="btn"  type="button"  onclick="eliminarpedidoconfirmar(' . $valor->Id_Pedido . ')"><i class="far fa-trash-alt col-sm-6 eliminari" id="" style="color:red;" href=""></i></a><br>'
                . '<a class="btn"  type="button"  onclick="infopedidoconfirmar(' . $valor->Id_Pedido . ')"><i class="fas fa-info-circle col-sm-6" style="color:#4e73df;"></i></a></td></tr>';
        }


        $this->load->view('dashboard/ventaConfirmarDashboard_v', array(
            'NombreUsuario' => $NombreUsuario,
            'ApellidoUsuario' => $ApellidoUsuario,
            'datosTablaPedidoc' => $datosTablaPedidoc,
            'CorreoUsuario' => $CorreoUsuario,
            'IdUsuario' => $IdUsuario,
            'ContrasenaUsuario' => $ContrasenaUsuario,
            'Numero' => $Numero,
            'Direccion' => $Direccion
        ));
    }


    function eliminarpedidoconfirmarc()
    {
        $idpedido = $this->input->post('pedidoid');
        $this->dashboard_m->eliminarpedidoconfirmarm($idpedido);
    }

    function confirmarpedidocad()
    {
        $idpedido = $this->input->post('pedidoid');
        $estado = $this->input->post('estado');
        $this->dashboard_m->confirmarpedidocadm($idpedido, $estado);
    }

    function vistaventasrealizadas()
    {

        $NombreUsuario = $this->session->userdata('NombreUsuario');
        $ApellidoUsuario = $this->session->userdata('ApellidoUsuario');
        $CorreoUsuario = $this->session->userdata('CorreoUsuario');
        $IdUsuario = $this->session->userdata('IdUsuario');
        $ContrasenaUsuario = $this->session->userdata('ContrasenaUsuario');
        $Numero = $this->session->userdata('Numero');
        $Direccion = $this->session->userdata('Direccion');

        $tablaconsultarpedidor = $this->dashboard_m->consultarventasr();


        $datosTablaPedidoc = '';


        foreach ($tablaconsultarpedidor as $valor) {

            $datosTablaPedidoc .=  '<tr>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Id_Usuario . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Nombre_Usuario . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Apellido . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Id_Producto . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Referencia . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Nombre . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Cantidad . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . number_format($valor->Valor_Total) . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Fecha_Pedido . '</td>'
                //                    .'<td>'.$valor->documento_profesional.'</td>'
                //     .'<td><button type="button" id="editarP" onclick="editarproducto('.$valor->Id_Producto.')"  class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-edit"></span> Editar </button>'
                // .'<button type="button" id="editarP"   class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-edit"></span> Eliminar </button></td>'

                . '<td align="center" id="" style="background-color: #E9F3F7; color:#000;"><a class="btn" onclick="consultarpedidorv(' . $valor->Id_Pedido . ')"><i class="fas fa-info-circle" style="color:#4e73df;"></i></a>'
                . '<a class="btn" onclick="estadodespachado(' . $valor->Id_Pedido . ')"><i class="fas fa-paper-plane" style="color:green;"></i></a></td></tr>';
        }



        $this->load->view('dashboard/ventaRealizadaDashboard_v', array(
            'NombreUsuario' => $NombreUsuario,
            'ApellidoUsuario' => $ApellidoUsuario,
            'datosTablaPedidoc' => $datosTablaPedidoc,
            'CorreoUsuario' => $CorreoUsuario,
            'IdUsuario' => $IdUsuario,
            'ContrasenaUsuario' => $ContrasenaUsuario,
            'Numero' => $Numero,
            'Direccion' => $Direccion

        ));
    }

    function consultarpedidorv()
    {
        $datos['NombreUsuario'] = $this->session->userdata('NombreUsuario');
        $datos['ApellidoUsuario'] = $this->session->userdata('ApellidoUsuario');
        $datos['CorreoUsuario'] = $this->session->userdata('CorreoUsuario');
        $datos['IdUsuario'] = $this->session->userdata('IdUsuario');
        $datos['ContrasenaUsuario'] = $this->session->userdata('ContrasenaUsuario');
        $datos['Numero'] = $this->session->userdata('Numero');
        $datos['Direccion'] = $this->session->userdata('Direccion');
        $idpedido = $this->input->get('idpedido');
        $tablaconsultarpedidoc = $this->dashboard_m->traerpedidocompleto($idpedido);





        foreach ($tablaconsultarpedidoc as $valor) {

            $datos['Id_Usuario'] = $valor->Id_Usuario;
            $datos['Nombre_Usuario'] = $valor->Nombre_Usuario;
            $datos['Apellido'] = $valor->Apellido;
            $datos['Numero'] = $valor->Numero;
            $datos['Direccion'] = $valor->Direccion;
            $datos['Correo'] = $valor->Correo;
            $datos['Id_Producto'] = $valor->Id_Producto;
            $datos['Nombre_Categoria'] = $valor->Nombre_Categoria;
            $datos['Referencia'] = $valor->Referencia;
            $datos['Descripcion'] = $valor->Descripcion;
            $datos['Nombre_Producto'] = $valor->Nombre_Producto;
            $datos['Precio_Unitario'] = $valor->Precio_Unitario;
            $datos['Cantidad'] = $valor->Cantidad;
            $datos['Valor_Total'] = number_format($valor->Valor_Total);
            $datos['Fecha_Pedido'] = $valor->Fecha_Pedido;
        }



        $this->load->view('dashboard/verpedidoindividual', $datos);
    }


    function vistainfoproducto()
    {
        $datos['NombreUsuario'] = $this->session->userdata('NombreUsuario');
        $datos['ApellidoUsuario'] = $this->session->userdata('ApellidoUsuario');
        $datos['CorreoUsuario'] = $this->session->userdata('CorreoUsuario');
        $datos['IdUsuario'] = $this->session->userdata('IdUsuario');
        $datos['ContrasenaUsuario'] = $this->session->userdata('ContrasenaUsuario');
        $datos['Numero'] = $this->session->userdata('Numero');
        $datos['Direccion'] = $this->session->userdata('Direccion');
        $idpedido = $this->input->get('idpedido');
        $datos['idpedido'] = $idpedido;
        $tablaeditarpedidoc = $this->dashboard_m->traerpedidocompletoe($idpedido);

        foreach ($tablaeditarpedidoc as $valor) {

            $datos['Id_Usuario'] = $valor->Id_Usuario;
            $datos['Nombre_Usuario'] = $valor->Nombre_Usuario;
            $datos['Apellido'] = $valor->Apellido;
            $datos['Numero'] = $valor->Numero;
            $datos['Direccion'] = $valor->Direccion;
            $datos['Correo'] = $valor->Correo;
            $datos['Id_Producto'] = $valor->Id_Producto;
            $datos['Nombre_Categoria'] = $valor->Nombre_Categoria;
            $datos['Referencia'] = $valor->Referencia;
            $datos['Descripcion'] = $valor->Descripcion;
            $datos['Nombre_Producto'] = $valor->Nombre_Producto;
            $datos['Precio_Unitario'] = $valor->Precio_Unitario;
            $datos['Cantidad'] = $valor->Cantidad;
            $datos['Valor_Total'] = number_format($valor->Valor_Total);
            $datos['Fecha_Pedido'] = $valor->Fecha_Pedido;
        }



        $this->load->view('dashboard/infopedidoindividual', $datos);
    }

    function estadodespachadoc()
    {
        $idpedido = $this->input->post('pedidoid');
        $estadopedido = $this->input->post('estado');

        $this->dashboard_m->estadodespachadom($idpedido, $estadopedido);
    }

    function vistaproductosdespachados()
    {

        $NombreUsuario = $this->session->userdata('NombreUsuario');
        $ApellidoUsuario = $this->session->userdata('ApellidoUsuario');
        $CorreoUsuario = $this->session->userdata('CorreoUsuario');
        $IdUsuario = $this->session->userdata('IdUsuario');
        $ContrasenaUsuario = $this->session->userdata('ContrasenaUsuario');
        $Numero = $this->session->userdata('Numero');
        $Direccion = $this->session->userdata('Direccion');

        $tablaconsultarpedidor = $this->dashboard_m->consultarventasdespachadasm();


        $datosTablaPedidoc = '';


        foreach ($tablaconsultarpedidor as $valor) {

            $datosTablaPedidoc .=  '<tr>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Id_Usuario . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Nombre_Usuario . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Apellido . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Id_Producto . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Referencia . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Nombre . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Cantidad . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . number_format($valor->Valor_Total) . '</td>'
                . '<td align="center" style="background-color: #E9F3F7; color:#000;">' . $valor->Fecha_Pedido . '</td>'
                //                    .'<td>'.$valor->documento_profesional.'</td>'
                //     .'<td><button type="button" id="editarP" onclick="editarproducto('.$valor->Id_Producto.')"  class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-edit"></span> Editar </button>'
                // .'<button type="button" id="editarP"   class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-edit"></span> Eliminar </button></td>'

                . '<td align="center" id="" style="background-color: #E9F3F7; color:#000;"><a class="btn" onclick="consultarpedidorv(' . $valor->Id_Pedido . ')"><i class="fas fa-info-circle" style="color:green;"></i></a>'
                . '</td></tr>';
        }



        $this->load->view('dashboard/vistaproductosdespachados', array(
            'NombreUsuario' => $NombreUsuario,
            'ApellidoUsuario' => $ApellidoUsuario,
            'datosTablaPedidoc' => $datosTablaPedidoc,
            'CorreoUsuario' => $CorreoUsuario,
            'IdUsuario' => $IdUsuario,
            'ContrasenaUsuario' => $ContrasenaUsuario,
            'Numero' => $Numero,
            'Direccion' => $Direccion

        ));
    }
    function actualizarpassuc()
    {
        $data = array(
            'usuarioid' => $this->input->post('usuarioid'),
            'contrasenaactual' => $this->input->post('contrasenaactual'),
            'contrasenanueva' => md5($this->input->post('contrasenanueva'))
            
        );

        $valusuarionpass = $this->usuario_m->validarcambiarpassum($data);

        if ($valusuarionpass) {
            $this->usuario_m->cambiarpassum($data);

            echo (1);
        } else {
            echo (0);
        }
    }

}
