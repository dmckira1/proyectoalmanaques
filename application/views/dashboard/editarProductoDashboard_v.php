<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">


  <title>Dashboard-Admin</title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url('resourcesAdmin/') ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?php echo base_url('resourcesAdmin/') ?>css/sb-admin-2.min.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url('resources/dropzone/dist'); ?>/dropzone.css"">

</head>

<body id=" page-top">
  <!-- ACA EMPIEZA EL MODAL DE EDITAR PERFIL -->
        <!-- ACA EMPIEZA EL MODAL -->
        <div class="container">
        <div class="modal" tabindex="-1" id="modal2">
            <!-- el tabindex="-1" es para poder cerrar la ventana presionando scape -->
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        INFORMACIÓN DE TU PERFIL
                        <button class="btn-danger" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" value="<?php echo $IdUsuario; ?>" id="apcidusuario" name="apcidusuario">
                        <div class="container">
                            <form action="">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Nombre(s)</label>
                                        <input class="form form-control" onkeyup="campovacioactualizard()" type="text" value="<?php echo $NombreUsuario ?>" id="apcnombre" name="apcnombre">  <hr>                                      
                                    </div>
                                
                                    <div class="col-md-6">
                                        <label for="">Apellidos</label>
                                        <input class="form form-control" onkeyup="campovacioactualizard()" type="text" value="<?php echo $ApellidoUsuario ?>" id="apcapellido" name="apcapellido"><hr>
                                    </div>
                                </div>                                      
                                   
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Correo</label>
                                        <input class="form form-control" onkeyup="campovacioactualizard()" type="text" value="<?php echo $CorreoUsuario ?>" id="apccorreo" name="apccorreo"><hr>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">Número de celular</label>
                                        <input class="form form-control" onkeyup="campovacioactualizard()" type="text" value="<?php echo $Numero ?>" id="apcnumero" name="apcnumero"><hr>
                                    </div>
                                </div>    

                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Dirección</label>
                                        <input class="form form-control" onkeyup="campovacioactualizard()" type="text" value="<?php echo $Direccion ?>" id="apcdireccion" name="apcdireccion"><hr>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="alert alert-danger" role="alert">
                                            Recuerdar que al modificar tu información, se cerrara la sesión para actualizar los cambios!
                                        </div><hr>
                                        </div>
                                </div>                                    
                                    <button id="btnactinpe" disabled="disabled" name="btnactinpe" class="btn btn-primary offset-10" type="button" onclick="actualizarusuariocliente()">Actualizar</button>
                                </div><br>  
                            </form>

                           <div class="container">
                                <form action="">
                                <!-- <div class="alert alert-info" role="alert">
                                    Recueda que !
                                </div> -->
                                    
                                    <h4>CAMBIO DE CONTRASEÑA</h4><hr width="730" size="">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="">Contraseña actual</label>
                                            <input class="form form-control" onkeyup="campovacioactualizarcontra()" type="password" value="" id="apccontrasenaa" name="apccontrasenaa"><hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="">Nueva contraseña</label>
                                            <input class="form form-control" onkeyup="campovacioactualizarcontra()" type="password" value="" id="apccontrasenan" name="apccontrasenan" placeholder=""><hr>
                                        </div>                           
                            
                                        <div class="col-md-6">
                                            <label for="">Repetir contraseña</label>
                                            <input class="form form-control" onkeyup="campovacioactualizarcontra()" type="password" id="apccontrasenar" value="" name="apccontrasenar" placeholder=""><hr>
                                        </div>
                                        
                                    </div>
                                    <button class="btn btn-primary offset-10" disabled="disabled" id="btncc" type="button" onclick="actualizarpasu()" >Actualizar</button>
                                </form>
                            </div> 
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- EL DATA DISSMIS PERMITE CERRAR LA VENTANA MODAL -->
                    </div>

                </div>

            </div>

        </div>
    </div>

  <!-- ACA TERMINA EL MODAL PARA EDITAR EL PERFIL -->
  
  <input type="text" id="idproductoep" name="idproductoep" style="display:none" value="<?php echo ($idproducto) ?>">
  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo site_url('c_dashboard') ?>">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">ADMINISTRADOR</div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo site_url('c_dashboard') ?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>INICIO</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Interface
      </div>

      <!-- Nav Item - Pages Collapse Menu -->

      <!-- Menu de venta -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-users"></i>
          <span>GESTIÓN USUARIOS</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <!-- <h6 class="collapse-header">Custom Components:</h6> -->
            <a class="collapse-item" href="<?php echo site_url('c_dashboard/usuario') ?>">Usuarios</a>
            <a class="collapse-item" href="<?php echo site_url('c_dashboard/usuarioi') ?>">Usuarios inactivos</a>
          </div>
        </div>
      </li>
      <!-- genstion de categorias   -->

      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCategorias" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fab fa-buffer"></i>
          <span>GESTIÓN CATEGORÍAS</span>
        </a>
        <div id="collapseCategorias" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">

            <a class="collapse-item" href="<?php echo site_url('c_dashboard/categorias') ?>">Categorias activas</a>
          </div>
        </div>
      </li>


      <!-- Nav Item - Utilities Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fab fa-product-hunt"></i>
          <span>GESTIÓN PRODUCTOS</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <!-- <h6 class="collapse-header">Custom Utilities:</h6> -->
            <a class="collapse-item" href="<?php echo site_url('c_dashboard/Producto') ?>">Productos disponibles</a>
            <a class="collapse-item" href="<?php echo site_url('c_dashboard/ProductoNo') ?>">Productos no disponibles</a>
          </div>
        </div>
      </li>
      <!-- Menu de venta -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseVentas" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-shopping-cart"></i>
          <span>GESTIÓN VENTAS</span>
        </a>
        <div id="collapseVentas" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <a class="collapse-item" href="<?php echo site_url('c_dashboard/vistaconfirmarventa') ?>">Ventas por confirmar</a>
            <a class="collapse-item" href="<?php echo site_url('c_dashboard/vistaventasrealizadas') ?>">Ventas realizadas</a>
            <a class="collapse-item" href="<?php echo site_url('c_dashboard/vistaproductosdespachados') ?>">Ventas despachadas</a>
          </div>
        </div>
      </li>



      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Otras
      </div>

       <!-- Nav Item - Pages Collapse Menu -->
       <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-folder"></i>
          <span>PÁGINAS</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Paginas principales:</h6>
            <a class="collapse-item" href="<?php echo site_url('login') ?>">Inicio</a>
            <a class="collapse-item" href="<?php echo site_url('login/vistaproductos') ?>">Productos</a>
            <a class="collapse-item" href="<?php echo site_url('login/vistacontactenos') ?>">Contáctenos</a>
            <a class="collapse-item" href="<?php echo site_url('login/vistaquienessomos') ?>">¿Quiénes somos?</a>
            <div class="collapse-divider"></div>
          </div>
        </div>
      </li>

      <!-- Nav Item - Charts -->
      <!-- <li class="nav-item">
        <a class="nav-link" href="charts.html">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Charts</span></a>
      </li> -->



      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>


    </ul>

    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>



          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>





            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- NAVEGADOR DE INFORMACION DE USUARIO -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $NombreUsuario . ' ' . $ApellidoUsuario ?></span>
                <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60">
              </a>
              <!-- CAJA DE TEXTO - INFORMACION DEL USAURO -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal2">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Perfil
                </a>
                <!-- <a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Configuración
                </a> -->
                <a class="dropdown-item" href="<?php echo site_url('login/index') ?>">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Regresar a la página principal
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Salir
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Aqui empieza el menu del dashboard -->
          <!-- Page Heading -->
          <div class="d-sm-flex align-items-center justify-content-between mb-4">

            <!-- Con este generamos el reporte -->
            <!-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download fa-sm text-white-50"></i> Generate Report</a> -->
          </div>
          <!-- aca empieza el contenido de la pagina-->
          <h2 align="center">Editar producto</h2><br><br>
          <hr><br>
          <div class="container">
            <center>
              <h5>Imagen actual</h5>
            </center>
            <center>
              <div class="">
                <center>
                  <img src="<?php echo base_url('resources/imagenesproductos/') ?><?php echo $Imagen ?>" class="" alt="" width="250" height="250" align="center"><br><br>
                </center>
              </div><br>
            </center>
            <form action="actualizarproductoconi" method="POST" enctype="multipart/form-data">
              <div class="row">
                <div class="row col-sm-6">
                  <label for="">Categoría</label>
                  <select required class="form form-control" name="apcategoria" id="apcategoria">
                    <option value="<?php echo $Id_Categoria; ?>"><?php echo $nombre_categoria; ?></option>
                    <?php echo ($mostrarcategoria) ?>

                  </select>
                  <input id="idproductoed" name="idproductoed" type="hidden" value="<?php  echo $idproducto ?>">

                </div>
                <hr>
                <div class="row col-sm-6">
                  <label for="">Referencia</label>
                  <input class="form form-control" id="apreferencia" name="apreferencia" type="text" value="<?php echo $Referencia; ?>" required>
                </div>
                <hr>
                <div class="row col-sm-6">
                  <label for="">Precio unitario</label>
                  <input class="form form-control" id="apprecioun" name="apprecioun" type="text" value="<?php echo $Precio_Unitario; ?>" required>
                </div>
                <hr>
                <div class="row col-sm-6">
                  <label for="">Nombre</label>
                  <input class="form form-control" id="apnombre" name="apnombre" type="text" value="<?php echo $Nombre; ?>" required>
                </div>
                <hr>
                <div class="row col-sm-6">
                  <label for="">Descripción</label>
                  <textarea class="form form-control" id="apdescripcion" name="apdescripcion" type="text" value="" required><?php echo $Descripcion; ?></textarea>
                </div>
                <hr>
                <hr>

                <div class="row col-sm-6">
                  <label for="">Cargar nueva imagen</label>
                  <input class="form form-control" name="upload2" type="file" value="" required>
                </div>
                <hr>
                <hr>
              </div><br>
              <input class="btn btn-primary" type="submit" value="Actualizar Producto">  
              <hr>
            </form>
          </div>



          <!-- Termina el contenido del menu -->
          <!-- End of Main Content -->

          <!-- Footer -->
          <footer class="sticky-footer bg-white">
            <div class="container my-auto">
              <div class="copyright text-center my-auto">
                <span>Copyright &copy; Your Website 2020</span>
              </div>
            </div>
          </footer>
          <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

      </div>
      <!-- End of Page Wrapper -->

      <!-- Scroll to Top Button-->
      <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
      </a>

      <!-- Logout Modal-->
      <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Listo para salir?</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close" href="">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">Selecciona "Salir" si desea cerrar sesión.</div>
            <div class="modal-footer">
              <button class="btn btn-danger" type="button" data-dismiss="modal">Cancelar</button>
              <a class="btn btn-primary" href="<?php echo site_url('login/logout') ?>">Salir</a>
            </div>
          </div>
        </div>
      </div>

      <!-- SWEETALERT2 -->
      <!-- SEETALERT2 -->
      <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
      <!-- //FONT AWESOME -->
      <script src="https://kit.fontawesome.com/0733cfcc2a.js" crossorigin="anonymous"></script>
      <!-- //VALIDACIONES -->
      <script src="<?php echo base_url('resources/') ?>js/validaciones.js"></script>

      <!-- Bootstrap core JavaScript-->
      <script src="<?php echo base_url('resourcesAdmin/') ?>vendor/jquery/jquery.min.js"></script>
      <script src="<?php echo base_url('resourcesAdmin/') ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

      <!-- Core plugin JavaScript-->
      <script src="<?php echo base_url('resourcesAdmin/') ?>vendor/jquery-easing/jquery.easing.min.js"></script>

      <!-- Custom scripts for all pages-->
      <script src="<?php echo base_url('resourcesAdmin/') ?>js/sb-admin-2.min.js"></script>

      <!-- Page level plugins -->
      <script src="<?php echo base_url('resourcesAdmin/') ?>vendor/chart.js/Chart.min.js"></script>

      <!-- Page level custom scripts -->
      <script src="<?php echo base_url('resourcesAdmin/') ?>js/demo/chart-area-demo.js"></script>
      <script src="<?php echo base_url('resourcesAdmin/') ?>js/demo/chart-pie-demo.js"></script>

      <!-- dropzone -->
      <script src="<?php echo base_url('resources/dropzone/dist'); ?>/dropzone.js"></script>

      <script type="text/javascript">
        Dropzone.options.listaimagenes = {
          dictDefaultMessage: "Selecciona o arrastra aquí la imagen del producto para asignar o cambiar la imagen actual",
          dictInvalidFileType: "Este tipo de archivos no es válido",
          acceptedFiles: 'image/*',
          maxFilesize: 50,
          timeout: 600000,
          success: function(file, response) {
            M.toast({
              html: 'La imagen del producto se ha actualizado correctamente!'
            })
            $("#fotoproducto").html(response);
          },
        };
      </script>
      </body>

</html>