<!DOCTYPE html>
<html lang="en">
<head>
	<title>Olvide Contraseña</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo base_url('resourcesLogin/') ?>images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form" method="POST" name="fregistro" id="fregistro">
					<span class="login100-form-title p-b-20">
						¿OLVIDASTE TU CONTRASEÑA?
                    </span><br><br><br>

                    <div class="centrarlo col-sm-12">
                        <label class="text-center" for="">INGRESE EL CORREO ELECTRONICO ASOCIADO</label>
						<input required class="form form-control text-sm-center" type="email" name="emailrecuperar" id="emailrecuperar" placeholder="Ingrese su correo"><hr>
                    </div>
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" type="button" onclick="recordarpass()">
							RECUPERAR
						</button>
					</div>

					<div class="w-full text-center p-t-27 p-b-239">

						<a href="<?= site_url('login') ?>" class="txt2">
							  Regresar a la pagina principal /
                        </a>
                        
                        <a href="<?= site_url('login/vistalogin') ?>" class="txt2">
							  Iniciar sesión
						</a>
					</div>

					
				</form>

				<div class="login100-more" style="background-image: url('<?php echo base_url('resourcesLogin/') ?>images/olvido.jpeg');"></div>
			</div>
		</div>
	</div>
	
	

	<div id="dropDownSelect1"></div>

	<script src="<?php echo base_url('resources/js/validaciones.js') ?>"></script>
	<!-- SEETALERT2 -->
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <!-- //FONT AWESOME -->
  <script src="https://kit.fontawesome.com/0733cfcc2a.js" crossorigin="anonymous"></script>
	
<!--===============================================================================================-->
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/select2/select2.min.js"></script>
	<script>
		$(".selection-2").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/daterangepicker/moment.min.js"></script>
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('resourcesLogin/') ?>js/main.js"></script>

</body>
</html>