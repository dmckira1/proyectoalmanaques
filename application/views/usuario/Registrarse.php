<!DOCTYPE html>
<html lang="en">
<head>
	<title>Registrarse</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo base_url('resourcesLogin/') ?>images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form validate-form"  name="fregistro" id="fregistro" action="login/registrarUsuario">
					<span class="login100-form-title p-b-20">
						FORMULARIO DE REGISTRO
                    </span><br><br><br>

                    <div class="col-sm-12">
                        <label class="sub-title01" for="">Nombre(s):</label>
						<input required class="form form-control text-sm-center" type="text" name="nombrer" id="nombrer" placeholder="Ingrese su nombre completo"><hr>
                    </div>
                    
                    <div class="col-sm-12">
                        <label class="text-center" for="">Apellidos:</label>
						<input required class="form form-control text-sm-center" type="text" name="apellidor" id="apellidor" value="" placeholder="Ingrese sus apellidos completos"><hr>
					</div>
					
					<div class="col-sm-12">
                        <label class="text-center" for="">Correo:</label>
						<input required class="form form-control text-sm-center" type="email" name="correoregistro" id="correoregistro" placeholder="Ingrese su correo electronico"><hr>
                    </div>
                    
					<div class="col-sm-6">
                        <label class="text-center" for="">Contraseña:</label>
						<input required class="form form-control text-sm-center" type="password" name="contrasenar" id="contrasenar" placeholder="Ingrese su contraseña"><hr>
                    </div>
                    <div class="col-sm-6">
					<label class="text-center" for="">Repetir Contraseña:</label>
						<input required class="form form-control text-sm-center" type="password" name="rcontrasenar" id="rcontrasenar" placeholder="Repita su constraseña"><hr>
					</div>
					
					<div class="container-login100-form-btn">
						<button id="fregistrar" name="fregistrar" class="login100-form-btn btnr" type="button" onclick="guardarUsuario()">
							Registrarse
						</button>
					</div>

					<div class="w-full text-center p-t-27 p-b-239">

						<a href="<?= site_url('login')  ?>" class="txt2">
							  Regresar a la pagina principal /
                        </a>
                        
                        <a href="<?= site_url('login/vistaLogin') ?>" class="txt2">
							  Iniciar sesión / 
						</a>

						<a href="<?= site_url('login/politicasdeseguridad') ?>" class="txt2">
							  Si continua con el registro aceptara la politica de la seguridad de datos
						</a>
					</div>

					
				</form>

				<div class="login100-more" style="background-image: url('<?php echo base_url('resourcesLogin/') ?>images/niño.jpeg');"></div>
			</div>
		</div>
	</div>
	
	

	<div id="dropDownSelect1"></div>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9.10.12/dist/sweetalert2.all.min.js"></script>   
 
	<script src="<?php echo base_url('resources/js/jquery-3.5.1.min.js') ?>"></script>

	<script src="<?php echo base_url('resources/js/validaciones.js') ?>"></script>
<!--===============================================================================================-->
	<!-- <script src="<?php //echo base_url('resourcesLogin/') ?>vendor/jquery/jquery-3.2.1.min.js"></script> -->
<!--===============================================================================================-->
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/select2/select2.min.js"></script>
	<script>
		$(".selection-2").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/daterangepicker/moment.min.js"></script>
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('resourcesLogin/') ?>js/main.js"></script>

</body>
</html>