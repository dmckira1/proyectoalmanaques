<!DOCTYPE html>
<html lang="en">

<head>
	<title>Recuperar contraseña</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="<?php echo base_url('resourcesLogin/') ?>images/icons/favicon.ico" />
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>vendor/bootstrap/css/bootstrap.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>fonts/font-awesome-4.7.0/css/font-awesome.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>vendor/animate/animate.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>vendor/css-hamburgers/hamburgers.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>vendor/animsition/css/animsition.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>vendor/select2/select2.min.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>vendor/daterangepicker/daterangepicker.css">
	<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>css/main.css">
	<!--===============================================================================================-->
</head>

<body>


	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form2 validate-form" method="POST" action="" id="fmr_login">
					<span class="login100-form-title p-b-34">
						Cambiar Contraseña	
					</span>


					<input class="" type="hidden" name="correocpass" id="correocpass" placeholder="Correo" value="<?php echo $_GET["vcorreo"] ?>">
					

					<input class="" type="hidden" name="contracpass" id="contracpass" placeholder="Contraseña" value="<?php echo $_GET["number"] ?>">

					
					<div class="wrap-input100 rs2-wrap-input100 validate-input m-b-20">
						<input class="input100" type="password" name="passrecu" id="passrecu" placeholder="Nueva Contraseña" value="">
						<span class="focus-input100"></span>
					</div>

					<div class="wrap-input100 rs2-wrap-input100 validate-input m-b-20">
						<input class="input100" type="password" name="vpassrecu" id="vpassrecu" placeholder="Verificar contraseña" value="">
						<span class="focus-input100"></span>
					</div>

					<div class="container-login100-form-btn">
						<button class="login100-form-btn" id="ingresar" onclick="crearnuevopas()" type="button">
							Ingresar
						</button>
					</div>

					<div class="w-full text-center p-t-27 p-b-239">
					

						

						
					</div>

				</form>

				<div class="login100-more" style="background-image: url('<?php echo base_url('resourcesLogin/') ?>images/portadalogin.jpg');"></div>
			</div>
		</div>
	</div>



	<div id="dropDownSelect1"></div>

	<!-- SEETALERT2 -->
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
	<!-- //FONT AWESOME -->
	<script src="https://kit.fontawesome.com/0733cfcc2a.js" crossorigin="anonymous"></script>
	<!-- //VALIDACIONES -->
	<script src="<?php echo base_url('resources/') ?>js/validaciones.js"></script>
	<!--===============================================================================================-->
	<script src="<?php echo base_url('resources/js/jquery-3.5.1.min.js') ?>"></script>
	<!--===============================================================================================-->
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/animsition/js/animsition.min.js"></script>
	<!--===============================================================================================-->
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/bootstrap/js/bootstrap.min.js"></script>
	<!--===============================================================================================-->
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/select2/select2.min.js"></script>
	<script>
		$(".selection-2").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script>
	<!--===============================================================================================-->
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/daterangepicker/moment.min.js"></script>
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/daterangepicker/daterangepicker.js"></script>
	<!--===============================================================================================-->
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/countdowntime/countdowntime.js"></script>
	<!--===============================================================================================-->
	<script src="<?php echo base_url('resourcesLogin/') ?>js/main.js"></script>
	<!--===============================================================================================-->
	<!-- <script src="<?php /*echo base_url('resources/js/auth/login.js')*/ ?>"></script> -->
	<script src="<?php echo base_url('resources/js/validaciones.js') ?>"></script>


	<script>


	</script>

</body>

</html>