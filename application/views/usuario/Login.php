<!DOCTYPE html>
<html lang="en">
<head>
	<title>Iniciar sesión</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo base_url('resourcesLogin/') ?>images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>vendor/animate/animate.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('resourcesLogin/') ?>css/main.css">
<!--===============================================================================================-->
</head>
<body>


	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form2 validate-form" method="POST" action="" id="fmr_login">
					<span class="login100-form-title p-b-34">
						Iniciar sesión
					</span>
					
					<div class="wrap-input100 rs1-wrap-input100 validate-input m-b-20" data-validate="Type user name">
						<input class="input100" type="email" name="username" id="username" placeholder="Correo">
						<span class="focus-input100"></span>
					</div>
					<div class="wrap-input100 rs2-wrap-input100 validate-input m-b-20" data-validate="Type password">
						<input class="input100" type="password" name="pass" id="pass" placeholder="Contraseña">
						<span class="focus-input100"></span>
					</div>
					
					<div class="container-login100-form-btn">
						<button class="login100-form-btn" id="ingresar" onclick="ingresa()" type="button">
							Ingresar
						</button>
					</div>

					<div class="w-full text-center p-t-27 p-b-239">
						<span class="txt1">
							¿Olvidaste
						</span>

						<a href="<?= site_url('login/olvido') ?>" class="txt2">
							  la contaseña?
						</a><br><br>

						<a href="<?=site_url('login')?>" class="txt2">
							Regresar a la página principal
						</a><br><br>

						<a href="<?= site_url('login/vistaregistro') ?>" class="txt3">
							Regístrate
						</a>
					</div>

				</form>

				<div class="login100-more" style="background-image: url('<?php echo base_url('resourcesLogin/') ?>images/portadalogin.jpg');"></div>
			</div>
		</div>
	</div>
	
	

	<div id="dropDownSelect1"></div>
	
<!-- SEETALERT2 -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
  <!-- //FONT AWESOME -->
  <script src="https://kit.fontawesome.com/0733cfcc2a.js" crossorigin="anonymous"></script>
  <!-- //VALIDACIONES -->
  <script src="<?php echo base_url('resources/') ?>js/validaciones.js"></script>
<!--===============================================================================================-->
<script src="<?php echo base_url('resources/js/jquery-3.5.1.min.js') ?>"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/select2/select2.min.js"></script>
	<script>
		$(".selection-2").select2({
			minimumResultsForSearch: 20,
			dropdownParent: $('#dropDownSelect1')
		});
	</script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/daterangepicker/moment.min.js"></script>
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('resourcesLogin/') ?>vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url('resourcesLogin/') ?>js/main.js"></script>
<!--===============================================================================================-->
<!-- <script src="<?php /*echo base_url('resources/js/auth/login.js')*/ ?>"></script> -->
<script src="<?php echo base_url('resources/js/validaciones.js') ?>"></script>


<script>


</script>

</body>
</html>