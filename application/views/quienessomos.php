<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo base_url('resources/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('resources/css/styles.css') ?>">
    <link href="https://fonts.googleapis.com/css2?family=Titillium+Web:wght@300;400;600&display=swap" rel="stylesheet">
    <!-- Ionic icons-->
    <link href="https://unpkg.com/ionicons@4.5.5/dist/css/ionicons.min.css" rel="stylesheet">
    <link href="<?php echo base_url('resourcesAdmin/') ?>vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <title>¿Quiénes somos?
    </title>
</head>

<body>
<a href="https://api.whatsapp.com/send?phone=573006704288&text=Hola!%20Estoy%20interesado%20en%20comunicarme%20con%20ustedes%20el%20d%C3%ADa%20de%20hoy." class="float" target="_blank">
<i class="fa fa-whatsapp my-float"></i></a>

<a href="<?php echo base_url('resources/catalogopdf/catalogodigital2021.pdf') ?>" download="" class="float2" target="_blank">
<i class="fas fa-download my-float2"></i><p style="color: black; margin-top: 8px;"></p></a>
    <!-- ACA EMPIEZA EL NAVBAR -->
    <nav class="navbar navbar-expand-lg fixed-top">
        <div class="container">
            <a class="navbar-brand" href=""><img src="<?php echo base_url('resources/imagenes/logon.png') ?>"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <ion-icon name="grid-outline"></ion-icon>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo site_url('login/cargainicio') ?>">Inicio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo site_url('login/productos') ?>">Productos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo site_url('login/vistacontactenos') ?>">Contáctenos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo site_url('login/vistaquienessomos') ?>">¿Quiénes somos?</a>
                    </li>
                    <!-- <li class="nav-item"><a class="nav-link" href="#" onclick="salir()">Cerrar Sesión</a></li> -->
                    <?php if ($this->session->userdata('IdUsuario') && $this->session->userdata('RolUsuario') == 1 || $this->session->userdata('RolUsuario') == 2) : ?>
                        <li class="nav-item"><a class="nav-link" href="<?= site_url('login/logout') ?>">Cerrar sesión</a></li>
                    <?php elseif ($this->session->userdata('RolUsuario') == 3) : ?>
                    <?php else : ?>
                        <li class="nav-item"><a class="nav-link" href="<?= site_url('login/vistaLogin') ?>">Iniciar sesión</a></li>
                        <!-- <li class="nav-item"><a class="nav-link" href="#">Iniciar Sesión</a></li> -->
                    <?php endif; ?>
                    <?php if ($this->session->userdata('RolUsuario') == 1 || $this->session->userdata('RolUsuario') == 2) : ?>
                        <li class="nav-item"><a class="nav-link" href="<?= site_url('c_dashboard/index') ?>">Administrar</a></li>
                    <?php elseif ($this->session->userdata('RolUsuario') == 3) : ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Ajustes
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="<?php echo site_url('login/consultarpedido') ?>">Mi pedido</a><hr>
                                <a class="dropdown-item" href="<?php echo site_url('login/enproceso')  ?>">En despacho</a><hr>
                                <a class="dropdown-item" href="<?php echo site_url('login/despachados')  ?>">Despachados</a><hr>
                                <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal2">Editar Perfil</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="<?= site_url('login/logout') ?>">Cerrar sesión</a>
                            </div>
                        </li>
                    <?php endif ?>


            </div>
        </div>
    </nav>

    <!-- ACA TERMINA EL NAVBAR -->
    <!-- ACA EMPIEZA EL MODAL DE EDITAR PERFIL -->
    <?php if ($this->session->userdata('IdUsuario') && $this->session->userdata('RolUsuario') == 3 ): ?>
        <div class="container">
        <div class="modal" tabindex="-1" id="modal2">
            <!-- el tabindex="-1" es para poder cerrar la ventana presionando scape -->
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        INFORMACIÓN DE TU PERFIL
                        <button class="btn-danger" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" value="<?php echo $IdUsuario; ?>" id="apcidusuario" name="apcidusuario">
                        <div class="container">
                            <form action="">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Nombre(s)</label>
                                        <input class="form form-control" type="text" value="<?php echo $NombreUsuario ?>" id="apcnombre" name="apcnombre">  <hr>                                      
                                    </div>
                                
                                    <div class="col-md-6">
                                        <label for="">Apellidos</label>
                                        <input class="form form-control" type="text" value="<?php echo $ApellidoUsuario ?>" id="apcapellido" name="apcapellido"><hr>
                                    </div>
                                </div>                                      
                                   
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Correo</label>
                                        <input class="form form-control" type="text" value="<?php echo $CorreoUsuario ?>" id="apccorreo" name="apccorreo"><hr>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">Número de celular</label>
                                        <input class="form form-control" type="text" value="<?php echo $Numero ?>" id="apcnumero" name="apcnumero"><hr>
                                    </div>
                                </div>    

                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Dirección</label>
                                        <input class="form form-control" type="text" value="<?php echo $Direccion ?>" id="apcdireccion" name="apcdireccion"><hr>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="alert alert-danger" role="alert">
                                            Recuerdar que al modificar tu información, se cerrara la sesión para actualizar los cambios!
                                        </div><hr>
                                        </div>
                                </div>                                    
                                    <button class="btn btn-primary offset-9" type="button" onclick="actualizarusuariocliente()">Actualizar</button>
                                </div><br>  
                            </form>

                           <div class="container">
                                <form action="">
                                <!-- <div class="alert alert-info" role="alert">
                                    Recueda que !
                                </div> -->
                                    
                                    <h4>CAMBIO DE CONTRASEÑA</h4><hr width="730" size="">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="">Contraseña actual</label>
                                            <input class="form form-control" type="password" value="" id="apccontrasenaa" name="apccontrasenaa"><hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="">Nueva contraseña</label>
                                            <input class="form form-control" type="password" value="" id="apccontrasenan" name="apccontrasenan" placeholder=""><hr>
                                        </div>                           
                            
                                        <div class="col-md-6">
                                            <label for="">Repetir contraseña</label>
                                            <input class="form form-control" type="password" id="apccontrasenar" value="" name="apccontrasenar" placeholder=""><hr>
                                        </div>
                                        
                                    </div>
                                    <button class="btn btn-primary offset-9" id="btncc" type="button" onclick="actualizarpasu()" >Actualizar</button>
                                </form>
                            </div> 
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- EL DATA DISSMIS PERMITE CERRAR LA VENTANA MODAL -->
                    </div>

                </div>

            </div>

        </div>
    </div>


    <?php endif ?>

    <!-- <section id="imagenprincipal1">
        <div class="container">
            <div class="content-center topmargin-lg"><br><br><br><br><br><br><br><br><br><br><br><br><br>
                <h1></h1> -->
                <!-- <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Doloremque saepe, magni iste, voluptate assumenda quasi dolorem dolores rem eveniet aut voluptas.</p> -->
                
            <!-- </div>
        </div>
    </section> -->



    <section id="aboutus" class="bg-Light-Grey divider">
        <div class="container">
            <div class="content-center2">
                <h2><b>Almanaques Carlos Vélez</b></h2>
                
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus necessitatibus laborum commodi totam, inventore vero distinctio. Ratione quo, praesentium inventore, id recusandae necessitatibus sint nemo aut minima autem eaque ducimus!
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate iure dolore perferendis eum ullam minus neque doloremque! Eius consectetur eligendi dolorum, autem, hic accusamus alias dolorem obcaecati minus, laborum magni.
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit. Voluptas quam possimus sapiente sint eveniet laudantium repudiandae obcaecati impedit inventore hic earum debitis quos officia, rem aperiam cum nostrum blanditiis dolorem.
                </p>
            </div>
            
            <!-- <div class="row">
                <div class="col-md-6 offset-3">
                    <div class="member-container">
                        <div class="member-details">
                            <h5>Carlos Vélez</h5>
                            <span>Director General</span>
                            <ul class="list-inline">
                                <li class="list-inline-item"><a href="#"><i class="icon ion-logo-instagram"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="icon ion-logo-facebook"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="icon ion-logo-whatsapp"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="icon ion-logo-twitter"></i></a></li>
                            </ul>
                        </div>
                        <img src="" width="" height="" class="img-fluid align="center" alt="member 1">
                    </div>
                </div>
            </div> -->
        </div>
    </section>

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        
                        <img src="<?php echo base_url('resources/imagenes/mision.jpeg') ?>" width="" height="" class="img-fluid align="center" alt="member 1"><br><br>
                    </div>
                    <div>
                    <h2 style="text-align: center;"><b>Nuestra Misión</b><br></h2>
                        <h4 align="center">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quos id voluptas corrupti, optio facilis error ipsam neque voluptates debitis quisquam, culpa nemo numquam! Consequatur corporis ab impedit harum non. Totam.
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eos neque, cupiditate nobis, illo quis maiores odio veniam culpa eaque repudiandae deserunt, at optio perferendis molestias tempore! Voluptatem ratione perferendis quod.
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Obcaecati, maxime repudiandae id magni aliquam fugit inventore quibusdam amet dolorum minima nam reiciendis dicta optio neque distinctio temporibus culpa, ipsam eveniet.
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos, adipisci repellendus a eligendi dolorem quae voluptates vero tempore, consequuntur saepe optio nobis commodi odit, reprehenderit sint eum doloribus debitis mollitia?
    </h4>
                    </div>
                </div>

            </div>
        </section>
        
        <section id="" class="bg-Light-Grey divider">
        <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        
                        <img src="<?php echo base_url('resources/imagenes/vision.jpeg') ?>" width="" height="" class="img-fluid align="center" alt="member 1"><br><br>
                    </div>
                    <div>
                    <h2 style="text-align: center;"><b>Nuestra Misión</b><br></h2>
                        <h4 align="center">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quos id voluptas corrupti, optio facilis error ipsam neque voluptates debitis quisquam, culpa nemo numquam! Consequatur corporis ab impedit harum non. Totam.
                            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eos neque, cupiditate nobis, illo quis maiores odio veniam culpa eaque repudiandae deserunt, at optio perferendis molestias tempore! Voluptatem ratione perferendis quod.
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Obcaecati, maxime repudiandae id magni aliquam fugit inventore quibusdam amet dolorum minima nam reiciendis dicta optio neque distinctio temporibus culpa, ipsam eveniet.
                            Lorem ipsum dolor sit amet consectetur adipisicing elit. Dignissimos, adipisci repellendus a eligendi dolorem quae voluptates vero tempore, consequuntur saepe optio nobis commodi odit, reprehenderit sint eum doloribus debitis mollitia?
    </h4>
                    </div>
                </div>

            </div>
    </section>

    <section id="footer" class="bg-dark">
        <div class="container">
            <img src="<?php echo base_url('resources/imagenes/logob.png') ?>">
            <ul class="list-inline">
                <li class="list-inline-item footer-menu">
                    <a href="<?php echo site_url('login/cargainicio') ?>"> Inicio </a>
                </li>
                <li class="list-inline-item footer-menu">
                    <a href="<?php echo site_url('login/productos') ?>"> Productos </a>
                </li>
                <li class="list-inline-item footer-menu">
                    <a href="<?php echo site_url('login/vistacontactenos') ?>"> Contáctenos </a>
                </li>
                <li class="list-inline-item footer-menu">
                    <a href="<?php echo site_url('login/vistaquienessomos') ?>"> ¿Quiénes somos? </a>
                </li>
            </ul>
            <ul class="list-inline">
                <li class="list-inline-item">
                    <a href=""><i class="icon ion-logo-instagram"></i></a>
                </li>
                <li class="list-inline-item">
                    <a href=""><i class="icon ion-logo-twitter"></i></a>
                </li>
                <li class="list-inline-item">
                    <a href=""><i class="icon ion-logo-facebook"></i></a>
                </li>
                <li class="list-inline-item">
                    <a href=""><i class="icon ion-logo-whatsapp"></i></a>
                </li>

            </ul>
            <small>©2020 System Almanaques.</small>
        </div>
    </section>


    <!-- SEETALERT2 -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <!-- //FONT AWESOME -->
    <script src="https://kit.fontawesome.com/0733cfcc2a.js" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/ionicons@5.1.2/dist/ionicons.js"></script>
    <script src="<?php echo base_url('resources/js/jquery-3.5.1.min.js') ?>"></script>
    <script src="<?php echo base_url('resources/js/bootstrap.min.js') ?>"></script>
    <script src="<?php echo base_url('resources/js/validaciones.js') ?>"></script>
    <script>
        $('#exampleModal').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var recipient = button.data('whatever') // Extract info from data-* attributes
            // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
            // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
            var modal = $(this)
            modal.find('.modal-title').text('New message to ' + recipient)
            modal.find('.modal-body input').val(recipient)
        })
    </script>
</body>

</html>