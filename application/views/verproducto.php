<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?php echo base_url('resources/css/bootstrap.min.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('resources/css/styles.css') ?>">
    <link rel="stylesheet" href="<?php echo base_url('resources/css/grid.css') ?>">
    <link href="https://fonts.googleapis.com/css2?family=Titillium+Web:wght@300;400;600&display=swap" rel="stylesheet">
    <!-- Ionic icons-->
    <link href="https://unpkg.com/ionicons@4.5.5/dist/css/ionicons.min.css" rel="stylesheet">
    
    <title>Inicio</title>
</head>
<body>

<!-- ACA EMPIEZA EL NAVBAR -->
<nav class="navbar navbar-expand-lg fixed-top">
        <div class="container">
            <a class="navbar-brand" href=""><img src="<?php echo base_url('resources/imagenes/logon.png') ?>"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <ion-icon name="grid-outline"></ion-icon>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo site_url('login/index') ?>">Inicio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo site_url('login/productos') ?>">Productos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo site_url('login/vistacontactenos') ?>">Contáctenos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo site_url('login/vistaquienessomos') ?>">¿Quiénes somos?</a>
                    </li>
                    <!-- <li class="nav-item"><a class="nav-link" href="#" onclick="salir()">Cerrar Sesión</a></li> -->
                    <?php if ($this->session->userdata('IdUsuario') && $this->session->userdata('RolUsuario') == 1 || $this->session->userdata('RolUsuario') == 2) : ?>
                        <li class="nav-item"><a class="nav-link" href="<?= site_url('login/logout') ?>">Cerrar sesión</a></li>
                    <?php elseif ($this->session->userdata('RolUsuario') == 3) : ?>
                    <?php else : ?>
                        <li class="nav-item"><a class="nav-link" href="<?= site_url('login/vistaLogin') ?>">Iniciar sesión</a></li>
                        <!-- <li class="nav-item"><a class="nav-link" href="#">Iniciar Sesión</a></li> -->
                    <?php endif; ?>
                    <?php if ($this->session->userdata('RolUsuario') == 1 || $this->session->userdata('RolUsuario') == 2) : ?>
                        <li class="nav-item"><a class="nav-link" href="<?= site_url('c_dashboard/index') ?>">Administrar</a></li>
                    <?php elseif ($this->session->userdata('RolUsuario') == 3) : ?>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Ajustes
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="<?php echo site_url('login/consultarpedido')  ?>">Mi pedido</a><hr>
                                <a class="dropdown-item" href="<?php echo site_url('login/enproceso')  ?>">En despacho</a><hr>
                                <a class="dropdown-item" href="<?php echo site_url('login/despachados')  ?>">Despachados</a><hr>
                                <a class="dropdown-item" href="#" type="button" data-toggle="modal" data-target="#modal1">Editar Perfil</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="<?= site_url('login/logout') ?>">Cerrar sesión</a>
                            </div>
                        </li>
                    <?php endif ?>


            </div>
        </div>
    </nav>


<!-- ACA TERMINA EL NAVBAR -->

<?php if ($this->session->userdata('IdUsuario') && $this->session->userdata('RolUsuario') == 3) : ?>
         <!-- ACA EMPIEZA EL MODAL -->
         <div class="container">
        <div class="modal" tabindex="-1" id="modal2">
            <!-- el tabindex="-1" es para poder cerrar la ventana presionando scape -->
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        INFORMACIÓN DE TU PERFIL
                        <button class="btn-danger" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" value="<?php echo $IdUsuario; ?>" id="apcidusuario" name="apcidusuario">
                        <div class="container">
                            <form action="">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Nombre(s)</label>
                                        <input class="form form-control" onkeydown="return sinNumerossiEspacio(event)" onkeyup="campovacioactualizard()" type="text" value="<?php echo $NombreUsuario ?>" id="apcnombre" name="apcnombre">  <hr>                                      
                                    </div>
                                
                                    <div class="col-md-6">
                                        <label for="">Apellidos</label>
                                        <input class="form form-control" onkeydown="return sinNumerossiEspacio(event)" onkeyup="campovacioactualizard()" type="text" value="<?php echo $ApellidoUsuario ?>" id="apcapellido" name="apcapellido"><hr>
                                    </div>
                                </div>                                      
                                   
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Correo</label>
                                        <input class="form form-control" onkeyup="campovacioactualizard()" type="text" value="<?php echo $CorreoUsuario ?>" id="apccorreo" name="apccorreo"><hr>
                                    </div>

                                    <div class="col-md-6">
                                        <label for="">Número de celular</label>
                                        <input class="form form-control" onkeyup="campovacioactualizard()" onkeydown=" return soloNumeros(event)" type="number" value="<?php echo $Numero ?>" id="apcnumero" name="apcnumero"><hr>
                                    </div>
                                </div>    

                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Dirección</label>
                                        <input class="form form-control" onkeyup="campovacioactualizard()" type="text" value="<?php echo $Direccion ?>" id="apcdireccion" name="apcdireccion"><hr>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="alert alert-danger" role="alert">
                                            Recuerda que al modificar tu información, se cerrara la sesión para actualizar los cambios!
                                        </div><hr>
                                        </div>
                                </div>                                    
                                    <button id="btnactinpe" disabled="disabled" name="btnactinpe" class="btn btn-primary offset-9" type="button" onclick="actualizarusuariocliente()">Actualizar</button>
                                </div><br>  
                            </form>

                           <div class="container">
                                <form action="">
                                <!-- <div class="alert alert-info" role="alert">
                                    Recueda que !
                                </div> -->
                                    
                                    <h4>CAMBIO DE CONTRASEÑA</h4><hr width="730" size="">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="">Contraseña actual</label>
                                            <input class="form form-control" onkeyup="campovacioactualizarcontra()" type="password" value="" id="apccontrasenaa" name="apccontrasenaa"><hr>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label for="">Nueva contraseña</label>
                                            <input class="form form-control" onkeyup="campovacioactualizarcontra()" type="password" value="" id="apccontrasenan" name="apccontrasenan" placeholder=""><hr>
                                        </div>                           
                            
                                        <div class="col-md-6">
                                            <label for="">Repetir contraseña</label>
                                            <input class="form form-control" onkeyup="campovacioactualizarcontra()" type="password" id="apccontrasenar" value="" name="apccontrasenar" placeholder=""><hr>
                                        </div>
                                        
                                    </div>
                                    <button class="btn btn-primary offset-9" disabled="disabled" id="btncc" type="button" onclick="actualizarpasu()" >Actualizar</button>
                                </form>
                            </div> 
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- EL DATA DISSMIS PERMITE CERRAR LA VENTANA MODAL -->
                    </div>

                </div>

            </div>

        </div>
    </div>

  <!-- ACA TERMINA EL MODAL PARA EDITAR EL PERFIL -->
    <?php endif ?>

<section id="portafolio" class="divider" ><br><br>


        <div class="contenedor">
        <div class="">
            <img style="" src="<?php echo base_url('resources/imagenesproductos/') ?><?php echo $Imagen ?>" alt="" width="400px" height="500px"> <br><br>
            <div class="row col-sm-6">
                <label class=""><h4><b>Nombre:</b></h4></label>
                <input type="text" id="nombrepp" name="nombrepp" readonly class="form form-control" style="background-color:transparent; border: 0;" value="<?php echo $Nombre ?>">
            </div><hr>
            <div class="row col-sm-6">            
                <label class=""><h4><b>Referencia: </b></h4></label>
                <input style="background-color:transparent; border: 0;" type="text" id="referenciapp" name="referenciapp" readonly class="form form-control" value="<?php echo $Referencia ?>">
            </div><hr>
        </div>
        <div>
       
            <div class="row col-sm-6">
                <label class=""><h4><b>Categoría: </b></h4></label>
                <input style="background-color:transparent; border: 0;" type="text" id="catepp" name="catepp" readonly class="form form-control" value="<?php echo $Nombre_Categoria ?>"><br>
            </div><hr>
            <div class="row col-sm-6">
                <label class=""><h4><b>Precio unitario: </b></h4></label>
                <input style="background-color:transparent; border: 0;" type="text" id="punitariopp" name="punitariopp" readonly class="form form-control" value="<?php echo $Precio_Unitario ?>"><br>
            </div><hr>
            <div class="row col-sm-12">            
                <label align="center"><h4><b>Descripción: </b></h4></label>
                <textarea style="background-color:transparent; border: 0;" type="text" readonly class="form form-control"> <?php echo $Descripcion ?> </textarea>
            </div><br>
            <a href="<?php echo site_url('login/productos')?>" type="btn" class="btn btn-primary offset-2">Regresar a los productos</a>

            
        </div>
    </div>
</section>
    <section id="footer" class="bg-dark">
        <div class="container">
            <img src="<?php echo base_url('resources/imagenes/logob.png') ?>">
                <ul class="list-inline">
                    <li class="list-inline-item footer-menu">
                        <a href="<?php echo site_url('login')?>"> Inicio </a>
                    </li>
                    <li class="list-inline-item footer-menu">
                        <a href="<?php echo site_url('login/productos')?>"> Productos </a>                        
                    </li>
                    <li class="list-inline-item footer-menu">
                        <a href="<?php echo site_url('login/contactenos')?>"> Contáctenos </a>                        
                    </li>
                    <li class="list-inline-item footer-menu">
                        <a href="<?php echo site_url('login/vistaquienessomos')?>"> ¿Quiénes somos? </a>
                    </li>
                </ul>
                <ul class="list-inline">
                    <li class="list-inline-item">
                        <a href=""><i class="icon ion-logo-instagram"></i></a>
                    </li>
                    <li class="list-inline-item">
                        <a href=""><i class="icon ion-logo-twitter"></i></a>
                    </li>
                    <li class="list-inline-item">
                        <a href=""><i class="icon ion-logo-facebook"></i></a>
                    </li>
                    <li class="list-inline-item">
                        <a href=""><i class="icon ion-logo-whatsapp"></i></a>
                    </li>

                </ul>
                <small>©2020 All Rights Reserved.</small>
        </div>
    </section>



<script src="https://unpkg.com/ionicons@5.1.2/dist/ionicons.js"></script>
<script src="<?php echo base_url('resources/js/jquery-3.5.1.min.js') ?>"></script>
<script src="<?php echo base_url('resources/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('resources/js/validaciones.js') ?>"></script>
</body>
</html>