<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Usuario_m extends CI_Model{

    function __construct(){
        parent::__construct();
        $this->load->database();
    }

//     public function login($usuario,$password){
//         //Si nos devuelve una fila es por que existe
//         $this->db->where('Correo',$usuario);
//         $this->db->where('Contrasena',$password);
        
//         $q = $this->db->get('usuario');
//         if($q->num_rows()>0)
//         {
//             return TRUE;
//         }
//         else
//         {
//             return FALSE;
//         }
//    }

public function validarusuario($data)
    {
      $this->db->where('Correo', $data['CorreoUsuario']);
      $this->db->where('Contrasena',$data['ContrasenaUsuario']);
       $query = $this->db->get('usuario');
       
         if($query->num_rows() > 0)
         {
                $resultado = $query->result();
               // $query->next_result(); //No se usa en esta version de codeigniter.
                $query->free_result(); //NO SE TOCAN  Estas dos funciones permiten realizar varias consultas en el mismo controlador.
                return $resultado;

         }
         else
         {
             return FALSE;
         }
   }

   public function validarusuariocambiarpass($data)
    {
      $this->db->where('Correo', $data['CorreoUsuario']);
      $this->db->where('Contrasena',$data['ContrasenaUsuario']);
       $query = $this->db->get('usuario');
       
         if($query->num_rows() > 0)
         {
                $resultado = $query->result();
               // $query->next_result(); //No se usa en esta version de codeigniter.
                $query->free_result(); //NO SE TOCAN  Estas dos funciones permiten realizar varias consultas en el mismo controlador.
                return $resultado;

         }
         else
         {
             return FALSE;
         }
   }

   public function validarcambiarpassum($data)
   {
       $contrasenaactual = $data['contrasenaactual'];
     $this->db->where('Id_Usuario', $data['usuarioid']);
     $this->db->where('Contrasena',md5($contrasenaactual));
      $query = $this->db->get('usuario');
      
        if($query->num_rows() > 0)
        {
               $resultado = $query->result();
              // $query->next_result(); //No se usa en esta version de codeigniter.
               $query->free_result(); //NO SE TOCAN  Estas dos funciones permiten realizar varias consultas en el mismo controlador.
               return $resultado;

        }
        else
        {
            return FALSE;
        }
  }
  public function cambiarpassum($data){
    
    $this->db->set('Contrasena', $data['contrasenanueva']);    
    // $data = array(
    //         'CorreoUsuario'=> $data['CorreoUsuario'],
    //         'Contrasena' => md5($ContrasenaNUEVA),
    //     );

$this->db->where('Id_Usuario', $data['usuarioid']);
$this->db->update('usuario');


}
    //put your code here
    public function cambiarpassdefini($data){
        $ContrasenaNUEVA = $data['contrasenausuarion'];
        $this->db->set('Contrasena', md5($ContrasenaNUEVA));    
        // $data = array(
        //         'CorreoUsuario'=> $data['CorreoUsuario'],
        //         'Contrasena' => md5($ContrasenaNUEVA),
        //     );

    $this->db->where('Correo', $data['CorreoUsuario']);
    $this->db->update('usuario');


    }


   //ESTE ES PARA OBTENER LOS USUARIOS E INSERTARLOS - VISTA DE REGISTRO DE USUARIOS.
    public function setUsuario($nombre,$apellido,$correo,$contraseña){

        $data = array(
            'Nombre' => $nombre,//Los de la izquierda son los nombres como estan en la bd
            'Apellido' => $apellido,
            'Correo' => $correo,
            'Contrasena' => md5($contraseña)
         );
         
         $this->db->insert('usuario', $data);


    }
    function verificarsicorreoex($correo){
        $query = $this->db->query('SELECT * FROM usuario WHERE Correo = "'.$correo.'"');
            return $query->row();

            
        if($query->num_rows() > 0)
        {
               $resultado = $query->result();
              // $query->next_result(); //No se usa en esta version de codeigniter.
               $query->free_result(); //NO SE TOCAN  Estas dos funciones permiten realizar varias consultas en el mismo controlador.
               return $resultado;

        }
        else
        {
            return FALSE;
        }

    }
    public function CantidadUsuarios(){
        $query = $this->db->get('usuario'); 
    }
    // function login($username,$password)
    // {
    //     //Si nos devuelve una fila es por que es correcto
    //    $this->db->where('Correo',$username);
    //    $this->db->where('Contrasena',$password);
    //    $q = $this->db->get('usuario');
    //    if($q->num_rows()>0)
    //    {
    //        return true;
    //    }else{
    //        return false;
    //    }
    // }
        function get_recordarPass($correo){
            $query = $this->db->query('SELECT Nombre, Apellido, Correo, Contrasena FROM usuario WHERE Correo = "'.$correo.'"');
            return $query->row();
            // $query = $this->db->get('usuario');
            // $this->db->where("Correo", $correo);

        
            // return  $query->row();


        }

        function get_validaLogin($correo){

            $this->db->select('Contrasena');
            $this->db->where("Correo", $correo);
            
            $query = $this->db->get('usuario');

            $contrasena = $query->result();

            return $contrasena;
        }
    }
