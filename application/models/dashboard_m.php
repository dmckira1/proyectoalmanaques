<?php
class Dashboard_m extends CI_Model{
    function __construct(){
        parent::__construct();
        $this->load->database();
    }
    function listaUsuarios(){

        $query = $this->db->query('SELECT u.Id_Usuario,r.Rol,u.Numero,u.Direccion,u.Nombre,u.Apellido,u.Correo
        FROM rol r JOIN usuario u
        ON r.Id_Rol = u.Id_Rol
        WHERE u.Id_Estado_Usuario = 1');
        $resultado = $query->result();
        $query->free_result(); //NO SE TOCAN  Estas dos funciones permiten realizar varias consultas en el mismo controlador.
        return $resultado;
    }
    function listaUsuariosi(){

        $query = $this->db->query('SELECT u.Id_Usuario,r.Rol,u.Numero,u.Direccion,u.Nombre,u.Apellido,u.Correo
        FROM rol r JOIN usuario u
        ON r.Id_Rol = u.Id_Rol
        WHERE u.Id_Estado_Usuario = 2');
        $resultado = $query->result();
        $query->free_result(); //NO SE TOCAN  Estas dos funciones permiten realizar varias consultas en el mismo controlador.
        return $resultado;
    }

    function listaProductos(){
        $this->db->select('Id_Producto,Id_Categoria,Id_Estado_Producto,Referencia,Nombre,Descripcion,Precio_Unitario,Imagen');
        $this->db->where("Id_Estado_Producto","1");
        $query = $this->db->get('producto');
        
        $resultado_Producto = $query->result();
        
         return $resultado_Producto;

    }

    function listaProductosn(){
        $this->db->select('Id_Producto,Id_Categoria,Id_Estado_Producto,Referencia,Nombre,Descripcion,Precio_Unitario,Imagen');
        $this->db->where("Id_Estado_Producto","2");
        $query = $this->db->get('producto');
        
        $resultado_Producto = $query->result();
        
         return $resultado_Producto;

    }
  

    function traercategoria(){
        $this->db->select('Id_Categoria,Nombre');
        $this->db->where("Id_Estado_Categoria","1");
        $query = $this->db->get('categoria');
        
        $categoria = $query->result();
        
         return $categoria;

    }

    function traerestado(){
        $query = $this->db->query('SELECT Id_Estado_Usuario, Estado
        FROM estado_usuario
        WHERE Id_Estado_Usuario <> 3');
        $resultado = $query->result();
        $query->free_result(); //NO SE TOCAN  Estas dos funciones permiten realizar varias consultas en el mismo controlador.
        return $resultado;

        // $this->db->select('Id_Estado_Usuario,Estado');
        // $this->db->where("Estado","3");
        // $query = $this->db->get('estado_usuario');
        
        // $estadou = $query->result();
        
        //  return $estadou;

    }

    function traerrol(){
        $this->db->select('Id_Rol,Rol');
        $query = $this->db->get('rol');
        
        $estadou = $query->result();
        
         return $estadou;
    }



    function actualizarPoductos(){
        $this->db->select('Id_Producto,Id_Categoria,Id_Estado_Producto,Referencia,Nombre,Descripcion,Imagen');
        $query = $this->db->get('producto');
        
        $resultado_Producto = $query->result();
        
         return $resultado_Producto;

    }

    function setProducto($categoria,$estado,$referencia,$nombre,$descripcion,$imagen){
        
        $data = array(
            'Id_Estado_Producto' => $estado,
            'Id_Categoria' => $categoria,
            'Referencia' => $referencia,
            'Nombre' => $nombre,
            'Descripcion' => $descripcion,
            'Imagen' => $imagen
         );
         
         $this->db->insert('producto', $data);
    }

    function categoriat($idcategoria){

        $this->db->where('Id_Categoria', $idcategoria);
        $query = $this->db->get('categoria');        
        $resultado_Categoria = $query->result();        
         return $resultado_Categoria;

    }

    function fmtraerproducto($idproducto){

        $query = $this->db->query('SELECT p.Id_Producto,p.Id_Categoria, p.Id_Estado_Producto,p.Referencia, p.Nombre,p.Descripcion, p.Imagen, p.Precio_Unitario, c.Nombre as nombre_categoria,e.Estado as nombre_estado FROM producto p 
        inner join categoria c on p.Id_Categoria = c.Id_Categoria inner join estado_producto e on p.Id_Estado_Producto = e.Id_Estado_Producto
        where p.Id_Producto = '.$idproducto.'');
        $resultado = $query->result();
        $query->free_result(); //NO SE TOCAN  Estas dos funciones permiten realizar varias consultas en el mismo controlador.
        return $resultado;

        // $this->db->select('Id_Producto,Id_Estado_Producto,Id_Categoria,Referencia,Nombre,Descripcion,Imagen');
        // $this->db->where('Id_Producto',$idproducto);
        // $query = $this->db->get('producto');
        
        // $producto = $query->result();
        
        //  return $producto;


    }

    function fmtraerusuario($idusuario){

        // $query = $this->db->query('SELECT p.Id_Producto,p.Id_Categoria, p.Id_Estado_Producto,p.Referencia, p.Nombre,p.Descripcion, p.Imagen, c.Nombre as nombre_categoria,e.Estado as nombre_estado FROM producto p 
        // inner join categoria c on p.Id_Categoria = c.Id_Categoria inner join estado_producto e on p.Id_Estado_Producto = e.Id_Estado_Producto
        // where p.Id_Producto = '.$idproducto.'');
        // $resultado = $query->result();
        // $query->free_result(); //NO SE TOCAN  Estas dos funciones permiten realizar varias consultas en el mismo controlador.
        // return $resultado;

        // // $this->db->select('Id_Producto,Id_Estado_Producto,Id_Categoria,Referencia,Nombre,Descripcion,Imagen');
        // // $this->db->where('Id_Producto',$idproducto);
        // // $query = $this->db->get('producto');
        
        // // $producto = $query->result();
        
        // //  return $producto;
        // $this->db->select('Id_Usuario,Id_Rol,Id_Estado_Usuario,Nombre,Apellido,Correo');
        // $this->db->where('Id_Usuario', $idusuario);
        // $query = $this->db->get('usuario');
        // $resultado = $query->result();
        // return $resultado;

       /* $this->db->select('usuario.Id_Usuario,usuario.Id_Rol,usuario.Id_Estado_Usuario,usuario.Nombre,usuario.Apellido,usuario.Correo,estado_usuario.Estado,rol.Rol');
        $this->db->where('usuario.Id_Usuario',$idusuario);
    $this->db->from('usuario');    
    $this->db->join('estado_usuario', 'estado_usuario.Id_Estado_Usuario = usuario.Id_Estado_Usuario');
    $this->db->join('rol', 'rol.Id_Rol = usuario.Id_Rol'); //LA SEGUNDA TABLA ES LA DE USUARIOS, EL DATO EN COMUN ENTRE LAS DOS TABLAS ES EL ID ROL
    $query = $this->db->get();
    return $query->row_array(); */

    $query = $this->db->query('SELECT usuario.Id_Usuario,usuario.Id_Rol,usuario.Id_Estado_Usuario,usuario.Numero,usuario.Direccion,usuario.Nombre,usuario.Apellido,usuario.Correo,estado_usuario.Estado,rol.Rol FROM usuario usuario 
        inner join estado_usuario estado_usuario on usuario.Id_Estado_Usuario = estado_usuario.Id_Estado_Usuario
         inner join rol rol  on usuario.Id_Rol = rol.Id_Rol
        where usuario.Id_Usuario = '.$idusuario.'');
        $resultado = $query->result();
        $query->free_result(); //NO SE TOCAN  Estas dos funciones permiten realizar varias consultas en el mismo controlador.
        return $resultado;


// Produces:
// SELECT * FROM blogs
// JOIN comments ON comments.id = blogs.id
    }

    function cambiardeestadousuariom($id,$estado){
        $data = array(
            'Id_Estado_Usuario' => $estado
         );
    $this->db->where('Id_Usuario',$id);
    $this->db->update('usuario',$data);
    }


    function listaCategorias(){
        $this->db->select('Id_Categoria,Id_Estado_Categoria,Nombre,Descripcion');
        $this->db->where("Id_Estado_Categoria","1");
        $query = $this->db->get('categoria');
        
        $resultado_Categoria = $query->result();
        
         return $resultado_Categoria;

    }
function setCategoria($categoria,$descripcion){
        
        $data = array(
      
            'Nombre' => $categoria,
            'Descripcion' => $descripcion,
         );
         
         $this->db->insert('categoria',$data);
    }

    function actualizarProducto($id,$categoria, $referencia, $nombre, $descripcion){

        $data = array(
            'Id_Categoria' => $categoria,
            'Referencia' => $referencia,
            'Nombre' => $nombre,
            'Descripcion' => $descripcion,
         );
            $this->db->where('Id_Producto',$id);
            $this->db->update('producto',$data);
    }

    function actualizarCategoriam($id,$nombre,$descripcion){
        $data = array(
            'Nombre' => $nombre,
            'Descripcion' => $descripcion,
         );
            $this->db->where('Id_Categoria',$id);
            $this->db->update('categoria',$data);
    }

    function insertarimagenesm($id_producto,$imagen){

        $data = array(
            'id_producto' => $id_producto,
            'imagen' => $imagen,
         );
         
         $this->db->insert('imagenesproducto', $data);

    }

    ////SECCION PARA EDITAR LOS USUARIOS
    function actualizarperfil($id,$nombre,$apellidos,$correo,$numero,$direccion){
        $data = array(
            'Nombre' => $nombre,
            'Apellido' => $apellidos,
            'Correo' => $correo,
            'Numero' => $numero,
            'Direccion' => $direccion,   
         );

    $this->db->where('Id_Usuario', $id);
    $this->db->update('usuario', $data);

    }

    function actualizarcontrasenam($id,$contrasena){
        $data = array(
            'Contrasena' => $contrasena   
         );

    $this->db->where('Id_Usuario', $id);
    $this->db->update('usuario', $data);

    }
    function actualizarusuariodesdeadminm($id,$rol,$estado,$nombre,$apellido,$correo){
        
        $data = array(
            'Id_Rol' => $rol,
            'Id_Estado_Usuario' => $estado,
            'Nombre' => $nombre,
            'Apellido' => $apellido,
            'Correo' => $correo
         );
    $this->db->where('Id_Usuario',$id);
    $this->db->update('usuario',$data);
    }
    function eliminarusuariom($id,$estado){
        $data = array(
            'Id_Estado_Usuario' => $estado
         );
    $this->db->where('Id_Usuario',$id);
    $this->db->update('usuario',$data);
    }
    ///TERMINA SECCION PARA EDITAR LOS USUARIOS 

    function eliminarproductom($id,$estado){
        $data = array(
            'Id_Estado_Producto' => $estado
         );
    $this->db->where('Id_Producto',$id);
    $this->db->update('producto',$data);
    }

    function cambiarnodisponiblem($id,$estado){
        $data = array(
            'Id_Estado_Producto' => $estado
         );
    $this->db->where('Id_Producto',$id);
    $this->db->update('producto',$data);
    }

    function cambiardisponiblem($id,$estado){
        $data = array(
            'Id_Estado_Producto' => $estado
         );
    $this->db->where('Id_Producto',$id);
    $this->db->update('producto',$data);
    }


    function eliminarcategoriam($id,$estado){
        $data = array(
            'Id_Estado_Categoria' => $estado
         );
    $this->db->where('Id_Categoria',$id);
    $this->db->update('categoria',$data);
    }

    function eliminarusuarioc($id,$estado){
        $data = array(
            'Id_Estado_Usuario' => $estado
         );
    $this->db->where('Id_Usuario',$id);
    $this->db->update('usuario',$data);
    }

    function actualizarimagenes($datos){
    $this->db->set('imagen', $datos['imagen']);
    $this->db->where('id_producto', $datos['id_producto']);
    $this->db->update('imagenesproducto');
    }

    function insertarimagenes($tabla, $datos){
        $query = $this->db->insert($tabla, $datos);
    }

    function listaproductosv(){
        $query = $this->db->query('SELECT p.Id_Producto, p.Referencia, p.Nombre, p.Descripcion, p.Precio_Unitario, p.Imagen, c.Nombre AS Nombre_categoria
        FROM producto p JOIN categoria c ON p.Id_Categoria = c.Id_Categoria
        WHERE p.Id_Estado_Producto = 1');
        $resultado = $query->result();
        $query->free_result(); //NO SE TOCAN  Estas dos funciones permiten realizar varias consultas en el mismo controlador.
        return $resultado;

    }

    function informacionpcm($idproducto){
        $query = $this->db->query('SELECT p.Id_Producto, p.Referencia, p.Nombre, p.Descripcion, p.Precio_Unitario, p.Imagen, c.Nombre AS Nombre_Categoria FROM producto p JOIN categoria c ON p.Id_Categoria = c.Id_Categoria 
        WHERE p.Id_Estado_Producto = 1
        AND p.Id_Producto ='.$idproducto.'');
        $resultado = $query->result();
        $query->free_result(); //NO SE TOCAN  Estas dos funciones permiten realizar varias consultas en el mismo controlador.
        return $resultado;

    }

    function agregarproductom($idusuario,$idproducto,$valor,$cantidad){
        $data = array(
            'Id_Usuario' => $idusuario,
            'Id_Producto' => $idproducto,
            'Valor_Total' => $valor,
            'Cantidad' => $cantidad
         );
         
         $this->db->insert('pedido', $data);
         return  $this->db->insert_id();
    }

    function consultarpedidom($idusuario){
        $query = $this->db->query('SELECT c.Nombre AS Categoria, p.Referencia, p.Nombre, p.Precio_Unitario, pe.Id_Pedido, pe.Cantidad , pe.Id_Estado_Pedido, pe.Valor_Total
        FROM categoria c JOIN producto p
        ON c.Id_Categoria = p.Id_Categoria
        JOIN pedido pe
        ON p.Id_Producto = pe.Id_Producto 
        WHERE pe.Id_Estado_Pedido = 3
        AND pe.Id_Usuario ='.$idusuario.'');
        $resultado = $query->result();
        $query->free_result(); //NO SE TOCAN  Estas dos funciones permiten realizar varias consultas en el mismo controlador.
        return $resultado;

    }

    function consultarpedidoenprocesom($idusuario){
        $query = $this->db->query('SELECT c.Nombre AS Categoria, p.Referencia, p.Nombre, p.Precio_Unitario, pe.Id_Pedido, pe.Cantidad , pe.Id_Estado_Pedido, pe.Valor_Total
        FROM categoria c JOIN producto p
        ON c.Id_Categoria = p.Id_Categoria
        JOIN pedido pe
        ON p.Id_Producto = pe.Id_Producto 
        WHERE pe.Id_Estado_Pedido = 1
        AND pe.Id_Usuario ='.$idusuario.'');
        $resultado = $query->result();
        $query->free_result(); //NO SE TOCAN  Estas dos funciones permiten realizar varias consultas en el mismo controlador.
        return $resultado;

    }

    function consultarpedidodespachadom($idusuario){
        $query = $this->db->query('SELECT c.Nombre AS Categoria, p.Referencia, p.Nombre, p.Precio_Unitario, pe.Id_Pedido, pe.Cantidad , pe.Id_Estado_Pedido, pe.Valor_Total
        FROM categoria c JOIN producto p
        ON c.Id_Categoria = p.Id_Categoria
        JOIN pedido pe
        ON p.Id_Producto = pe.Id_Producto 
        WHERE pe.Id_Estado_Pedido = 4
        AND pe.Id_Usuario ='.$idusuario.'');
        $resultado = $query->result();
        $query->free_result(); //NO SE TOCAN  Estas dos funciones permiten realizar varias consultas en el mismo controlador.
        return $resultado;

    }

    function contaradmin(){

        $query = $this->db->query('SELECT COUNT(*) FROM usuario WHERE Id_Rol = 1');
        $resultado = $query->result();
        $query->free_result(); //NO SE TOCAN  Estas dos funciones permiten realizar varias consultas en el mismo controlador.
        return $resultado;
        
    }

    function eliminarmipedidom($idpedido){
        $this->db->delete('pedido', array('Id_Pedido' => $idpedido));
    }

    function guardaproductoim($nombre,$categoria, $referencia, $descripcion, $imagen){
        $data = array(
            'Imagen' => $imagen
         );
         
         $this->db->insert('producto', $data);
    }

    function sumarvalorpedidom($id_usuario){
        $query = $this->db->query('SELECT SUM(Valor_Total) AS Valor_Total
        FROM pedido 
        WHERE Id_Estado_Pedido = 3 
        AND Id_Usuario = '.$id_usuario.'');
        $resultado = $query->result();
        $query->free_result(); //NO SE TOCAN  Estas dos funciones permiten realizar varias consultas en el mismo controlador.
        return $resultado;
    }

    function confirmarpedidom($idusuario){
      $this->db->query('UPDATE pedido SET Id_Estado_Pedido = 2 WHERE Id_Usuario = ' . $idusuario . ' AND Id_Estado_Pedido = 3');
    }

    function actualizarcmodalm($idpedido, $cantidad, $valor){
        // $this->db->query('UPDATE pedido SET Id_Estado_Pedido = 2 WHERE Id_Usuario = ' . $idusuario . ' AND Id_Estado_Pedido = 3');
        $data = array(
            'Cantidad' => $cantidad,
            'Valor_Total' => $valor
         );

        $this->db->where('Id_Pedido', $idpedido);
        $this->db->update('pedido', $data);
    }

    function cargarproductop($categoria, $referencia, $nombre, $descripcion, $preciounitario, $imagen){
        $data = array(
            'Id_Categoria' => $categoria,
            'Referencia' => $referencia,
            'Nombre' => $nombre,
            'Descripcion' => $descripcion,
            'Precio_Unitario' => $preciounitario,
            'Imagen' => $imagen
         );
         
         $this->db->insert('producto', $data);
    }

    function actualizarproductopi($idproducto,$categoria, $referencia, $nombre, $descripcion, $preciounitario, $imagen){
        $data = array(
            'Id_Categoria' => $categoria,
            'Referencia' => $referencia,
            'Nombre' => $nombre,
            'Descripcion' => $descripcion,
            'Precio_Unitario' => $preciounitario,
            'Imagen' => $imagen
         );
         $this->db->where('Id_Producto', $idproducto);
         $this->db->update('producto', $data);
    }

    function agregarproductodetallem($id_pedido, $producto){
        $data = array(
            'title' => 'My title' ,
            'name' => 'My Name' ,
            'date' => 'My date'
         );
    }


    function consultarpedidoc(){
        $query = $this->db->query('SELECT u.Id_Usuario, u.Nombre, u.Apellido, u.Correo, pr.Referencia, pr.Nombre AS Nombre_Producto, pe.Id_Pedido, pe.Cantidad, pe.Valor_Total, pe.Fecha_Pedido FROM usuario u JOIN pedido pe ON u.Id_Usuario = pe.Id_Usuario JOIN producto pr ON pe.Id_Producto = pr.Id_Producto WHERE pe.Id_Estado_Pedido = 2');
        $resultado = $query->result();
        $query->free_result(); //NO SE TOCAN  Estas dos funciones permiten realizar varias consultas en el mismo controlador.
        return $resultado;
    }

    function confirmarpedidocadm($idpedido,$estado){
        $data = array(
            'Id_Estado_Pedido' => $estado 
         );
         $this->db->where('Id_Pedido', $idpedido);
        $this->db->update('pedido', $data);
    }



    function consultarventasr(){
        $query = $this->db->query('SELECT u.Id_Usuario, u.Nombre AS Nombre_Usuario, u.Apellido, u.Correo, pr.Id_Producto, pr.Referencia, pr.Nombre, pe.Id_Pedido, pe.Cantidad, pe.Valor_Total, pe.Fecha_Pedido
        FROM usuario u JOIN pedido pe 
        ON u.Id_Usuario = pe.Id_Usuario JOIN producto pr
        ON pe.Id_Producto = pr.Id_Producto
        WHERE pe.Id_Estado_Pedido = 1');
        $resultado = $query->result();
        $query->free_result(); //NO SE TOCAN  Estas dos funciones permiten realizar varias consultas en el mismo controlador.
        return $resultado;
    }

    function consultarventasdespachadasm(){
        $query = $this->db->query('SELECT u.Id_Usuario, u.Nombre AS Nombre_Usuario, u.Apellido, u.Correo, pr.Id_Producto, pr.Referencia, pr.Nombre, pe.Id_Pedido, pe.Cantidad, pe.Valor_Total, pe.Fecha_Pedido
        FROM usuario u JOIN pedido pe 
        ON u.Id_Usuario = pe.Id_Usuario JOIN producto pr
        ON pe.Id_Producto = pr.Id_Producto
        WHERE pe.Id_Estado_Pedido = 4');
        $resultado = $query->result();
        $query->free_result(); //NO SE TOCAN  Estas dos funciones permiten realizar varias consultas en el mismo controlador.
        return $resultado;
    }

    function traerpedidocompleto($idpedido){
        $query = $this->db->query('SELECT u.Id_Usuario, u.Nombre AS Nombre_Usuario, u.Apellido, u.Correo, u.Numero, u.Direccion, pe.Id_Pedido, pe.Cantidad, pe.Valor_Total, pe.Fecha_Pedido, pr.Id_Producto, pr.Referencia, pr.Descripcion, pr.Nombre AS Nombre_Producto, pr.Precio_Unitario, c.Nombre AS Nombre_Categoria 
        FROM usuario u JOIN pedido pe 
        ON u.Id_Usuario = pe.Id_Usuario JOIN producto pr 
        ON pe.Id_Producto = pr.Id_Producto JOIN categoria c 
        ON pr.Id_Categoria = c.Id_Categoria 
        WHERE pe.Id_Pedido = '. $idpedido .'');
        $resultado = $query->result();
        $query->free_result(); //NO SE TOCAN  Estas dos funciones permiten realizar varias consultas en el mismo controlador.
        return $resultado;
    }

    function eliminarpedidoconfirmarm($idpedido){

        $this->db->delete('pedido', array('Id_Pedido' => $idpedido));

    }

    function traerpedidocompletoe($idpedido){
        $query = $this->db->query('SELECT u.Id_Usuario, u.Nombre AS Nombre_Usuario, u.Apellido, u.Correo, u.Numero, u.Direccion, pe.Id_Pedido, pe.Cantidad, pe.Valor_Total, pe.Fecha_Pedido, pr.Id_Producto, pr.Referencia, pr.Descripcion, pr.Nombre AS Nombre_Producto, pr.Precio_Unitario, c.Nombre AS Nombre_Categoria 
        FROM usuario u JOIN pedido pe 
        ON u.Id_Usuario = pe.Id_Usuario JOIN producto pr 
        ON pe.Id_Producto = pr.Id_Producto JOIN categoria c 
        ON pr.Id_Categoria = c.Id_Categoria 
        WHERE pe.Id_Pedido = '. $idpedido .'');
        $resultado = $query->result();
        $query->free_result(); //NO SE TOCAN  Estas dos funciones permiten realizar varias consultas en el mismo controlador.
        return $resultado;
    }

    function estadodespachadom($idpedido, $estadopedido){

        $data = array(
            'Id_Estado_Pedido' => $estadopedido
         );
    $this->db->where('Id_Pedido',$idpedido);
    $this->db->update('pedido',$data);

    }

}