function guardarUsuario() {
    var nombre = $('#nombrer').val();
    var apellido = $('#apellidor').val();
    var correo = $('#correoregistro').val();
    var contrasena = $('contrasenar').val();
    var rcontrasena = $('rcontrasenar').val();

    if (nombre == '' && apellido == '' && correo == '' && contrasena == '' && rcontrasena == '') {
        // alert('Debe llenar todos los campos');
        Swal.fire({
            icon: 'error',
            title: '',
            text: 'DEBES LLENAR TODOS LOS CAMBIOS'
        });

    } else if (contrasena == '' != rcontrasena == '') {
        Swal.fire({
            icon: 'error',
            title: '',
            text: 'LAS CONTRASEÑAS NO COINCIDEN'
        });
        // alert('Las contraseñas no coinciden');
    } else if (nombre == '') {
        Swal.fire({
            icon: 'error',
            title: 'NOMBRE VACIO',
            text: 'Debes introducir un nombre para poder continuar con el registro'
        });
    } else if (apellido == '') {
        Swal.fire({
            icon: 'error',
            title: 'APELLIDO VACIO',
            text: 'Debes introducir un apellido para poder continuar con el registro'
        });
    } else if (correo == '') {
        Swal.fire({
            icon: 'error',
            title: 'CORREO VACIO',
            text: 'Debes introducir un correo para poder continuar con el registro'
        });
    } else if ((nombre != '' && apellido != '' && correo != '') && ($('#contrasenar').val() == $('#rcontrasenar').val())) {


        $.ajax({
            url: "guardarUsuarioC", //aqui llamo el nombre de la fiuncion del controlador.
            type: "POST",
            data: {
                nombre: $('#nombrer').val(), //El de la izquierda lo llamo como quiera y el de la derecha lo llamo como el id
                apellido: $('#apellidor').val(),
                correo: $('#correoregistro').val(),
                contraseña: $('#contrasenar').val(),
            },
            dataType: "html",
            success: function(obj) {

                if (obj == '1') {

                    Swal.fire({
                        icon: 'success',
                        title: '',
                        text: 'SE HA REGISTRADO EXITOSAMENTE    '
                    });
                    location.href = "vistaLogin";


                } else {

                    // alert('No entra');
                    //mensajito('NO VALIDO', 'Datos incorrectos');


                    Swal.fire({
                        icon: 'error',
                        title: '',
                        text: 'El correo ingresado ya esta registrado'
                    });


                }
            }





        });
    } else if ($('#contrasenar').val() != $('#rcontrasenar').val()) {
        Swal.fire({
            icon: 'error',
            title: 'CONTRASEÑAS NO COINCIDEN',
            text: 'Por favor verifica que ambas contraseñas sean las mismas para poder continuar con el registro'
        });
    }
}

//---------------CAMBIAR CONTRASEÑA--------------------------------------------------------------------------------------------------------------------
function actualizarpasu() {
    if ($('#apccontrasenan').val() !== $('#apccontrasenar').val()) {
        Swal.fire({
            icon: 'error',
            title: 'El campo "Nueva contraseña" no coincide con el campo "Repetir contraseña".',
            text: ''
        });
    } else {

        $.ajax({
            url: "actualizarpassuc",
            type: "POST",
            data: {
                usuarioid: $('#apcidusuario').val(),
                contrasenaactual: $('#apccontrasenaa').val(),
                contrasenanueva: $('#apccontrasenan').val()
            },
            dataType: "html",
            success: function(obj) {


                if (obj == '1') {
                    Swal.fire({
                        icon: 'success',
                        title: 'Su contraseña se a actualizado.',
                        text: ''
                    });

                    // window.location = "../login/cargainicio";

                } else {

                    Swal.fire({
                        icon: 'error',
                        title: 'DATOS INCORRECTOS',
                        text: 'La contraseña actual no coincide'
                    });

                }
            }
        });
    }
}


// --------------------------------------------------------------------------------------------------------------

function ingresa() {
    var correo = $('#username').val();
    var contrasena = $('#pass').val();
    if (correo == '') {
        Swal.fire({
            icon: 'error',
            title: 'CORREO VACIO',
            text: 'Por favor introduce un correo para poder ingresar'
        });
    } else if (contrasena == '') {
        Swal.fire({
            icon: 'error',
            title: 'CONTRASEÑA VACIA',
            text: 'Por favor introduce una contraseña para poder ingresar'
        });
    } else if (contrasena == '' && correo == '') {
        Swal.fire({
            icon: 'error',
            title: 'CAMPOS VACIOS',
            text: 'Por favor llena los campos para poder ingresar'
        });
    } else {
        $.ajax({
            url: "fc_login",
            type: "POST",
            data: {
                CorreoUsuario: $('#username').val(),
                ContrasenaUsuario: $('#pass').val()
            },
            dataType: "html",
            success: function(obj) {

                if (obj == '1') {


                    window.location = "../login/cargainicio";

                } else {

                    // alert('No entra');
                    //mensajito('NO VALIDO', 'Datos incorrectos');
                    Swal.fire({
                        icon: 'error',
                        title: 'DATOS INCORRECTOS',
                        text: 'Verifica que los datos subministrados esten correctos para poder acceder'
                    });

                }
            }
        });
    }
}

function crearnuevopas() {
    var pass1 = $('#passrecu').val();
    var pass2 = $('#vpassrecu').val();
    if (pass1 == '') {
        Swal.fire({
            icon: 'error',
            title: 'La contraseña esta vacia',
            text: 'Por favor introduce una contraseña para poder actualizarla'
        });
    } else if (pass2 == '') {
        Swal.fire({
            icon: 'error',
            title: 'La verificacion de la contraseña esta vacia',
            text: 'Por favor introduce una contraseña para poder ingresar'
        });
    } else if (pass1 == '' && pass2 == '') {
        Swal.fire({
            icon: 'error',
            title: 'CAMPOS VACIOS',
            text: 'Por favor llena los campos para poder actualizar su información'
        });
    } else if (pass1 != pass2) {
        Swal.fire({
            icon: 'error',
            title: 'Las contraseñas no coinciden',
            text: 'Por favor verifique que las contraseñas sean iguales'
        });
    } else {
        $.ajax({
            url: "cambiarpassw",
            type: "POST",
            data: {
                CorreoUsuario: $('#correocpass').val(),
                ContrasenaUsuario: $('#contracpass').val(),
                contrasenausuarion: $('#passrecu').val()
            },
            dataType: "html",
            success: function(obj) {

                Swal.fire({
                    icon: 'success',
                    title: 'Contraseña actualizada correctamente',
                    text: ''
                });
                if (obj == '1') {


                    window.location = "../login/cargainicio";

                } else {

                    Swal.fire({
                        icon: 'error',
                        title: 'DATOS INCORRECTOS',
                        text: 'Verifica que los datos subministrados esten correctos para poder acceder'
                    });

                }
            }
        });
    }
}

function salir() {
    $.ajax({


        url: "logout",
        type: "GET",
        data: {},
        dataType: "html",
        success: function(obj) {


        }
    });
}


// ----------------------------------------------------------------------------------------------------------


//ESTE ES PARA EL BUSCADOR DE LOS USUARIOS EN LA VISTA usuarioDashboard_v 
// $(document).ready(function() {
//     $(".search").keyup(function() {
//         var searchTerm = $(".search").val();
//         var listItem = $('.results tbody').children('tr');
//         var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

//         $.extend($.expr[':'], {
//             'containsi': function(elem, i, match, array) {
//                 return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
//             }
//         });

//         $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function(e) {
//             $(this).attr('visible', 'false');
//         });

//         $(".results tbody tr:containsi('" + searchSplit + "')").each(function(e) {
//             $(this).attr('visible', 'true');
//         });

//         var jobCount = $('.results tbody tr[visible="true"]').length;
//         $('.counter').text(jobCount + ' item');

//         if (jobCount == '0') { $('.no-result').show(); } else { $('.no-result').hide(); }
//     });

// });
//AQUI TERMINA EL BUSCADOR DE LOS USUARIOS

// -----------------------------------------------------------------------------------------------------------------

//SOLO MAYUSCULAS
function mayus(e) {
    e.value = e.value.toUpperCase();
}

// ---------------------------------------------------------------------------------------------------------

// function validarcamposproducto() {

//     if ($('#rpcategoria').val() !== '' && $('#rpreferencia').val() !== '' && $('#rpnombre').val() !== '' && $('#rpdescripcion').val() !== '' && $('#listaimagenes').val() !== '') {

//         //alert('entra habilitar');
//         $('#frenviar').prop('disabled', false);

//     } else {
//         //alert('entra');
//         $('#frenviar').prop('disabled', true);
//     }
// }

function guardarProducto() {


    $.ajax({
        url: "guardarProductoC", //aqui llamo el nombre de la fiuncion del controlador.
        type: "POST",
        data: {
            categoria: $('#rpcategoria').val(),
            estado: $('#rpestado').val(),
            referencia: $('#rpreferencia').val(), //El de la izquierda lo llamo como quiera y el de la derecha lo llamo como el id
            nombre: $('#rpnombre').val(),
            descripcion: $('#rpdescripcion').val(),
            imagen: $('#fileimagen').val()

        },
        dataType: "html",
        success: function(obj) {
            // alert('Se ha registrado exitosamente');
            Swal.fire({
                icon: 'success',
                title: '',
                text: 'SE HA REGISTRADO EXITOSAMENTE    '
            });



            location.href = "Producto"; //Este es el controlador

            //window.open ("http://www.familiamedellin.com.co/modulo_inventario/modulo_cif/index.php/c_datossd/fc_datossd?documento="+ $('#documentousuario').val() + "&docaux="+$('#docprofesional').val()+"&opcion=0","nuevo", "directories=no, location=no, menubar=no, scrollbars=yes, statusbar=no, tittlebar=no, width=400, height=400"); 

            //window.open("http://www.familiamedellin.com.co/modulo_inventario/modulo_cif/index.php/c_datossd/fc_datossd?documento="+ $('#documentousuario').val() + "&docaux="+$('#docprofesional').val()+"&opcion=0", "_blank")



        }

    });


}

function editarcategoria() {
    $.ajax({
        url: "actualizarCategoriaC", //aqui llamo el nombre de la fiuncion del controlador.
        type: "POST",
        data: {
            id: $('#idcategoriaa').val(),
            nombre: $('#ecnombre').val(),
            descripcion: $('#ecdescripcion').val(), //El de la izquierda lo llamo como quiera y el de la derecha lo llamo como el id
        },
        dataType: "html",
        success: function(obj) {
            // alert('Se ha registrado exitosamente');

            Swal.fire({
                icon: 'success',
                title: '',
                text: 'SE HA EDITADO EXITOSAMENTE    '
            });

            location.href = "Categorias"; //Esta es la vista

        }

    });
}

function editarProducto() {

    $.ajax({
        url: "actualizarProductoC", //aqui llamo el nombre de la fiuncion del controlador.
        type: "POST",
        data: {
            id: $('#idproductoep').val(),
            categoria: $('#apcategoria').val(),
            referencia: $('#apreferencia').val(), //El de la izquierda lo llamo como quiera y el de la derecha lo llamo como el id
            nombre: $('#apnombre').val(),
            descripcion: $('#apdescripcion').val(),
        },
        dataType: "html",
        success: function(obj) {
            // alert('Se ha registrado exitosamente');

            Swal.fire({
                icon: 'success',
                title: '',
                text: 'SE HA EDITADO EXITOSAMENTE    '
            });



            //  location.href = "Producto"; //Esta es la vista
        }

    });
}


// -----------------------------------------------------------------------------------------------------------

//CATEGORIAS--------------------------

//EMPIEZA EL INSERTAR CATEGORIA

function guardarCategoria() {
    $.ajax({
        url: "guardarCategoriaC", //aqui llamo el nombre de la fiuncion del controlador.
        type: "POST",
        data: {

            categoria: $('#nombrecrearc').val(),
            descripcionn: $('#descripcioncrearc').val()

        },
        dataType: "html",
        success: function(obj) {

            Swal.fire({
                icon: 'success',
                title: '',
                text: 'SE HA REGISTRADO EXITOSAMENTE'
            });

            location.href = "categorias"; //Esta es la vista

        }

    });

}

function irvistaEditarProducto(idproducto) {
    location.href = "vistaEditarProducto?idproducto=" + idproducto;
}

function irvistaEditarCategoria(idcategoria) {
    location.href = "vistaEditarCategoria?idcategoria=" + idcategoria;
}

function irvistaEditarUsuario(idusuario) {
    location.href = "vistaEditarUsuario?idusuario=" + idusuario;
}

function eliminarusuario(idusuario) {

    Swal.fire({
        title: 'Estas seguro de eliminar este usuario?',
        text: "No podras revertir esta acción!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si!',
        cancelButtonText: 'No!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: "eliminarusuarioc", //aqui llamo el nombre de la fiuncion del controlador.
                type: "POST",
                data: {

                    usuarioid: idusuario,
                    estado: 3

                },
                dataType: "html",
                success: function(obj) {

                    Swal.fire(
                        'Eliminado!',
                        'Se ellimino el usuario seleccionado.',
                        'success'
                    );

                    location.href = "Usuario"; //Esta es la vista

                }

            });


        }
    })
}

function eliminarproducto(idproducto) { //recibe el ID del producto que se manda desde la funcion ONCLICK que esta desde el controlador en el foreach de producto 
    Swal.fire({
        title: 'Estas seguro de eliminar este producto?',
        text: "No podras revertir esta acción!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si!',
        cancelButtonText: 'No!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: "eliminarproductoc", //aqui llamo el nombre de la fiuncion del controlador.
                type: "POST",
                data: {

                    productoid: idproducto, //Nombre del input: valor que se le agrega al input 
                    estado: 3 // valor de input 

                },
                dataType: "html",
                success: function(obj) { // lo que se cumple en la condicion de AJAX

                    Swal.fire(
                        'Eliminado!',
                        'Se ellimino el producto seleccionado.',
                        'success'
                    );

                    location.href = "Producto"; //Esta es la vista

                }

            });


        }
    })
}

function cambiarnodisponible(idproducto) { //recibe el ID del producto que se manda desde la funcion ONCLICK que esta desde el controlador en el foreach de producto 
    Swal.fire({
        title: 'Estas seguro de cambiar a "No disponible" este producto?',
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si!',
        cancelButtonText: 'No!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: "cambiarnodisponiblec", //aqui llamo el nombre de la fiuncion del controlador.
                type: "POST",
                data: {

                    productoid: idproducto, //Nombre del input: valor que se le agrega al input 
                    estado: 2 // valor de input 

                },
                dataType: "html",
                success: function(obj) { // lo que se cumple en la condicion de AJAX

                    Swal.fire(
                        'Proceso exitoso!',
                        'Se cambio el producto a "No disponible".',
                        'success'
                    );

                    location.href = "Producto"; //Esta es la vista

                }

            });


        }
    })
}

function cambiardisponible(idproducto) { //recibe el ID del producto que se manda desde la funcion ONCLICK que esta desde el controlador en el foreach de producto 
    Swal.fire({
        title: 'Estas seguro de cambiar a "Disponible" este producto?',
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si!',
        cancelButtonText: 'No!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: "cambiardisponiblec", //aqui llamo el nombre de la fiuncion del controlador.
                type: "POST",
                data: {

                    productoid: idproducto, //Nombre del input: valor que se le agrega al input 
                    estado: 1 // valor de input 

                },
                dataType: "html",
                success: function(obj) { // lo que se cumple en la condicion de AJAX

                    Swal.fire(
                        'Proceso exitoso!',
                        'Se cambio el producto a "Disponible".',
                        'success'
                    );

                    location.href = "ProductoNo"; //Esta es la vista

                }

            });


        }
    })
}


function eliminarcategoria(idcategoria) {

    Swal.fire({
        title: 'Estas seguro de eliminar esta categoria?',
        text: "No podras revertir esta acción!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si!',
        cancelButtonText: 'No!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: "eliminarcategoriac", //aqui llamo el nombre de la fiuncion del controlador.
                type: "POST",
                data: {

                    categoriaid: idcategoria,
                    estado: 2
                },
                dataType: "html",
                success: function(obj) {

                    Swal.fire(
                        'Eliminado!',
                        'Se ellimino la categoria seleccionada.',
                        'success'
                    );

                    location.href = "Categorias"; //Esta es la vista

                }

            });


        }
    })
}
//TERMINA EL INSERTAR CATEGORIA--------------------------------------------------------------------------------


//ESTA FUNCION ES PARA ACTUALIZAR LAS DATOS DEL USUARIO

function actualizarusuario() {

    $.ajax({
        url: "actualizarperfilc", //aqui llamo el nombre de la fiuncion del controlador.
        type: "POST",
        data: {

            usuarioid: $('#apdidusuario').val(),
            nombre: $('#apdnombre').val(),
            apellidos: $('#apdapellidos').val(),
            correo: $('#apdcorreo').val(),
            contrasena: $('#apdcontraseña').val()

        },
        dataType: "html",
        success: function(obj) {

            Swal.fire({
                icon: 'success',
                title: '',
                text: 'SE HA ACTUALIZADO SU INFORMACIÓN CORRECTAMENTE'
            });


            location.href = "Usuario"; //Esta es la vista

        }

    });
}

function actualizarusuariodesdeadmin() {

    $.ajax({
        url: "actualizarusuariodesdeadminc", //aqui llamo el nombre de la fiuncion del controlador.
        type: "POST",

        data: {
            usuarioid: $('#auaidusuario').val(),
            rol: $('#auarol').val(),
            estado: $('#auaestado').val(),
            nombre: $('#auanombre').val(),
            apellidos: $('#auaapellido').val(),
            correo: $('#auacorreo').val(),
        },
        dataType: "html",
        success: function(obj) {
            Swal.fire({
                icon: 'success',
                title: '',
                text: 'SE ACTULIZO ESTE USUARIO CORRECTAMENTE',
            });


            location.href = "Usuario"; //Esta es la vista

        }

    });

}

function actualizarusuariocliente() {
    $.ajax({
        url: "actualizarperfilcc", //aqui llamo el nombre de la funcion del controlador.
        type: "POST",
        data: {
            idusuario: $('#apcidusuario').val(),
            nombre: $('#apcnombre').val(),
            apellidos: $('#apcapellido').val(),
            correo: $('#apccorreo').val(),
            numero: $('#apcnumero').val(),
            direccion: $('#apcdireccion').val(),
            // contrasena: $('#apccontrasena').val(),
        },
        dataType: "html",
        success: function(obj) {

            Swal.fire({
                icon: 'success',
                title: '',
                text: 'SE HA ACTUALIZADO SU INFORMACIÓN CORRECTAMENTE'
            });

            location.href = "logout"; //Esta es la vista

        }

    });
}



function elliminarusuario(idusuario) {
    $.ajax({
        url: "cambiardeestadousuario", //aqui llamo el nombre de la funcion del controlador.
        type: "POST",
        data: {
            idusuario: $('#iddusuario').val(idusuario),
            estado: $('#estadodusuario').val('2'),
        },
        dataType: "html",
        success: function(obj) {

            Swal.fire({
                icon: 'success',
                title: '',
                text: 'SE HA ACTUALIZADO SU INFORMACIÓN CORRECTAMENTE'
            });

            location.href = ""; //Esta es la vista

        }

    });
}

function olvidopass() {

    $.ajax({
        url: "resetlink", //aqui llamo el nombre de la funcion del controlador.
        type: "POST",
        data: {
            correo: $('#emailrecuperar').val()
        },
        dataType: "html",
        success: function(obj) {

            Swal.fire({
                icon: 'success',
                title: '',
                text: 'SE HA ENVIADO LA SOLICITUD AL CORREO SUMINISTRADO'
            });

            location.href = ""; //Esta es la vista

        }

    });

}

function actualizarcontrasena() {

    if ($('#contrausuarioc').val() == '' || $('#apccontrasenan').val() == '' || $('#apccontrasenar').val() == '') {

        Swal.fire({
            icon: 'error',
            title: '',
            text: 'DEBE COMPLETAR TODOS LOS CAMPOS'
        });

    } else if ($('#contrausuarioc').val() !== $('#apccontrasenaa').val()) {

        Swal.fire({
            icon: 'error',
            title: '',
            text: 'LA CONTRASEÑA ACTUAL NO COINCIDE CON LA QUE DIGITO'
        });
    } else if ($('#apccontrasenan').val() !== $('#apccontrasenar').val()) {
        Swal.fire({
            icon: 'error',
            title: '',
            text: 'LAS CONTRASEÑAS NO COINCIDEN'
        });
    } else {
        $.ajax({
            url: "actualizarcontrasenac", //aqui llamo el nombre de la funcion del controlador.
            type: "POST",
            data: {
                idusuario: $('#apcidusuario').val(),
                contrasena: $('#apccontrasenan').val()
            },
            dataType: "html",
            success: function(obj) {

                Swal.fire({
                    icon: 'success',
                    title: '',
                    text: 'SE HA ACTUALIZADO SU INFORMACIÓN CORRECTAMENTE'
                });

                location.href = "logout"; //Esta es la vista

            }

        });

    }
}

function informacionp(idproducto) {
    $.ajax({
        url: "informacionpc", //aqui llamo el nombre de la funcion del controlador.
        type: "POST",
        data: {
            idproducto: idproducto,
        },
        dataType: "html",
        success: function(obj) {

            Swal.fire({
                icon: 'success',
                title: '',
                text: 'SE HA ENVIADO LA SOLICITUD AL CORREO SUMINISTRADO'
            });

            location.href = ""; //Esta es la vista

        }

    });
}

function vistaverproducto(idproducto) {
    location.href = "vistaverproducto?idproducto=" + idproducto;
}

function insertarpedido() {
    $.ajax({
        url: "insertarpedidoc", //aqui llamo el nombre de la funcion del controlador.
        type: "POST",
        data: {
            idusuario: $('#idusuariopp').val(),
            referncia: $('#referenciapp').val(),
            cantidad: $('#valorpp').val(),

            // contrasena: $('#apccontrasena').val(),
        },
        dataType: "html",
        success: function(obj) {

            Swal.fire({
                icon: 'success',
                title: '',
                text: 'Su pedido se ha cargado en "Mi pedido" correctamente.'
            });


        }

    });
}

function agregarproducto(idproducto) {
    $.ajax({
        url: "agregarproductoc", //aqui llamo el nombre de la funcion del controlador.
        type: "POST",
        data: {
            idusuario: $('#idusuariopp').val(),
            idproducto: idproducto,
            valor: $('#valorpp' + idproducto).val(),
            cantidad: $('#cantidadpp' + idproducto).val(),

        },
        dataType: "html",
        success: function(obj) {

            Swal.fire({
                icon: 'success',
                title: '',
                text: 'Su pedido se ha cargado en "Mi pedido" correctamente.'
            });


        }

    });

}

function sumarvtproductos() {
    $('#')
}

function valortotal(idproducto) {

    var v1 = $('#punitariopp' + idproducto).val();
    var v2 = $('#cantidadpp' + idproducto).val();
    var Total = v1 * v2;
    $('#valorpp' + idproducto).val(Total);
}

function eliminarmipedido(idpedido) {

    Swal.fire({
        title: '¿Desea eliminar este producto de su lista?',
        text: "No podras revertir esta acción!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si!',
        cancelButtonText: 'No!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: "eliminarmipedidoc", //aqui llamo el nombre de la funcion del controlador.
                type: "POST",
                data: {

                    pedidoid: idpedido,

                },
                dataType: "html",
                success: function(obj) {

                    Swal.fire(
                        'Eliminado!',
                        'Se ellimino el pedido seleccionado.',
                        'success'
                    );

                    location.href = "consultarpedido"; //Esta es la vista

                }

            });


        }
    })
}

function sumarvalorpedido(usuarioidvp) {
    $.ajax({
        url: "consultarpedido", //aqui llamo el nombre de la funcion del controlador.
        type: "POST",
        data: {
            idusuariovp: usuarioidvp,


        },
        dataType: "html",
        success: function(obj) {

        }

    });

}

function confirmarpedido() {
    $.ajax({
        url: "confirmarpedidoc", //aqui llamo el nombre de la fiuncion del controlador.
        type: "POST",
        data: {
            idusuario: $('#idvusuario').val() //El de la izquierda lo llamo como quiera y el de la derecha lo llamo como el id
        },
        dataType: "html",
        success: function(obj) {
            // alert('Se ha registrado exitosamente');

            Swal.fire({
                icon: 'success',
                title: '',
                text: 'SE HA CONFIRMADO SU PEDIDO EXITOSAMENTE '
            });

            location.href = "consultarpedido"; //Esta es la vista
        }

    });
}

$('#modal20').on('show.bs.modal', function(event) {
    var button = $(event.relatedTarget) // Botón que activó el modal
    var pedido = button.data('id_pedido') // Extraer la información de atributos de datos
    var cantidad = button.data('cantidadpm')
    var referencia = button.data('referenciapm')
    var categoria = button.data('categoriapm')
    var preciounitario = button.data('precioupm')

    var modal = $(this)
    modal.find('.modal-body #id_pedido').val(pedido),
        modal.find('.modal-body #cantidadpm').val(cantidad),
        modal.find('.modal-body #referenciapm').val(referencia),
        modal.find('.modal-body #categoriapm').val(categoria),
        modal.find('.modal-body #precioupm').val(preciounitario)


    $('.alert').hide(); //Oculto alert
})

function valortotalmodal() {

    var v1 = $('#precioupm').val();
    var v2 = $('#cantidadpm').val();
    var Total = v1 * v2;
    $('#vtotalmodal').val(Total);
}

function actualizarcmodal() {
    $.ajax({
        url: "actualizarcmodalc", //aqui llamo el nombre de la funcion del controlador.
        type: "POST",
        data: {
            idpedido: $('#id_pedido').val(),
            cantidad: $('#cantidadpm').val(),
            valor: $('#vtotalmodal').val()


        },
        dataType: "html",
        success: function(obj) {

            Swal.fire({
                icon: 'success',
                title: '',
                text: 'Su pedido se ha actualzado correctamente.'
            });

            location.href = "consultarpedido"; //Esta es la vista
            $('#cantidadpm').val("");
        }


    });
}

function datosproductoprueba() {
    $.ajax({
        url: "cargarimagen", //aqui llamo el nombre de la funcion del controlador.
        type: "POST",
        data: {
            categoria: $('#categoriaprueba').val(),
            referencia: $('#referenciaprueba').val(),
            preciounitario: $('#preciounitarioprueba').val(),
            nombre: $('#nombreprueba').val(),
            descipcion: $('#descripcionprueba').val()


        },
        dataType: "html",
        success: function(obj) {

            Swal.fire({
                icon: 'success',
                title: '',
                text: 'Su pedido se ha actualzado correctamente.'
            });

        }


    });
}

function confirmarpedidoad(idpedido) {
    Swal.fire({
        title: '¿Desea confirmar que tiene disponibilidad de este producto?',
        text: "No podras revertir esta acción!",
        icon: 'success',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si!',
        cancelButtonText: 'No!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: "confirmarpedidocad", //aqui llamo el nombre de la funcion del controlador.
                type: "POST",
                data: {

                    pedidoid: idpedido,
                    estado: 1

                },
                dataType: "html",
                success: function(obj) {

                    Swal.fire(
                        'Confirmado!',
                        'Se confirmo el pedido seleccionado.',
                        'success'
                    );

                    location.href = "vistaconfirmarventa"; //Esta es la vista

                }

            });


        }
    })
}

function consultarpedidorv(idpedido) {
    location.href = "consultarpedidorv?idpedido=" + idpedido;
}

function eliminarpedidoconfirmar(idpedido) {
    Swal.fire({
        title: 'Desea eliminar este pedido?',
        text: "No podras revertir esta acción!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si!',
        cancelButtonText: 'No!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: "eliminarpedidoconfirmarc", //aqui llamo el nombre de la fiuncion del controlador.
                type: "POST",
                data: {

                    pedidoid: idpedido,


                },
                dataType: "html",
                success: function(obj) {

                    Swal.fire(
                        'Eliminado!',
                        'Se ellimino el pedido seleccionado.',
                        'success'
                    );

                    location.href = "vistaconfirmarventa"; //Esta es la vista

                }

            });


        }
    })
}


function infopedidoconfirmar(idpedido) {
    location.href = "vistainfoproducto?idpedido=" + idpedido;
}

function recordarpass() {
    $.ajax({
        url: "recuperarpass", //Aqui llama a la funcion php q busca por documento los datos de la persona en la tabla de login, incluyendo la contraseña para desencriptar
        type: "post",
        data: {
            correo: $('#emailrecuperar').val(),

        },
        dataType: "json",
        success: function(data) {
            if (data == '1') {

                Swal.fire(
                    'Error!',
                    'El correo no existe',
                    'error'
                );
            } else {

                mailpass(data.nombre, data.correo, data.pass);
            }

        }

    });
}

function mailpass(nombre, correo, pass) {
    $.ajax({
        url: "fc_mailrecordarpass",
        type: "post",
        data: {
            vnombre: nombre,
            vcorreo: correo,
            contrasena: pass
        },
        dataType: "html",
        success: function(data) {

            Swal.fire(
                'Enviado!',
                'Se ha enviado la contraseña al correo electrónico: ' + correo + '\n Por favor validar!!',
                'success'
            );

        }


    });
}

function estadodespachado(idpedido) {

    Swal.fire({
        title: '¿Desea confirmar el despacho de este pedido?',
        text: "No podras revertir esta acción!",
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si!',
        cancelButtonText: 'No!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: "estadodespachadoc", //aqui llamo el nombre de la funcion del controlador.
                type: "POST",
                data: {

                    pedidoid: idpedido,
                    estado: 4

                },
                dataType: "html",
                success: function(obj) {

                    Swal.fire(
                        'Confirmado!',
                        'Se confirmo el despacho del pedido seleccionado.',
                        'success'
                    );

                    location.href = "vistaventasrealizadas"; //Esta es la vista

                }

            });


        }
    })

}


// VALIDACIONES DE CAMPOS
function sinNumerossiEspacio(e) {

    var key = window.Event ? e.which : e.keyCode
    if ((key < 48 || key > 57) || (key === 8)) {

    } else {
        Swal.fire({
            icon: 'info',
            title: 'Recuerda!',
            text: 'En este campo solo puedes ingresar letras'
        });
    }
    //alert(key);
    return ((key < 48 || key > 57) || (key === 8))
}

function soloNumeros(e) {
    //    alert(e);

    var key = window.Event ? e.which : e.keyCode
    if ((key >= 48 && key <= 57) || (key == 8)) {} else {
        Swal.fire(
            'Recuerda!',
            'En este campo solo puedes ingresar numeros',
            'info'
        );
    }
    return ((key >= 48 && key <= 57) || (key == 8))
}

function campovaciop(idproducto) {
    if ($('#cantidadpp' + idproducto).val() !== '') {
        $('#btnap' + idproducto).prop('disabled', false);
    } else {
        $('#btnap' + idproducto).prop('disabled', true);
    }

}

function campovaciocategoria() {
    if ($('#nombrecrearc').val() !== '' && $('#descripcioncrearc').val() !== '') {
        $('#btnenviarcategoria').prop('disabled', false);
    } else {
        $('#btnenviarcategoria').prop('disabled', true);
    }

}

function campovacioagcategoria() {
    if ($('#ecnombre').val() !== '' && $('#ecdescripcion').val() !== '') {
        $('#edcatbtn').prop('disabled', false);
    } else {
        $('#edcatbtn').prop('disabled', true);
    }

}

function campovacioproducto() {
    if ($('#nombreprueba').val() !== '' && $('#descripcioncrearc').val() !== '') {
        $('#btnenviarcategoria').prop('disabled', false);
    } else {
        $('#btnenviarcategoria').prop('disabled', true);
    }

}

function campovacioactualizard() {
    let nombre = $('#apcnombre').val();
    let apellido = $('#apcapellido').val();
    let correo = $('#apccorreo').val();
    let numero = $('#apcnumero').val();
    let direccion = $('#apcdireccion').val();
    if (nombre !== '' && apellido !== '' && correo !== '' && numero !== '' && direccion !== '') {
        $('#btnactinpe').prop('disabled', false);
    } else {
        $('#btnactinpe').prop('disabled', true);
    }

}

function campovacioactualizarcontra() {

    if ($('#apccontrasenaa').val() !== '' && $('#apccontrasenan').val() && $('#apccontrasenar').val() !== '') {
        $('#btncc').prop('disabled', false);
    } else {
        $('#btncc').prop('disabled', true);
    }

}

function campovaciomipedido() {

    if ($('#cantidadpm').val() !== '') {
        $('#btnenviarupdatemodal').prop('disabled', false);
    } else {
        $('#btnenviarupdatemodal').prop('disabled', true);
    }

}